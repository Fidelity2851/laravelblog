<?php

namespace App\Http\Controllers;

use App\category;
use App\Http\Controllers\Controller;
use App\post;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index(){
        /*Header category*/
        $cate = category::with('subcategory')->get();

        /*Banner posts*/
        $banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(3)->get();
        $small_banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->inRandomOrder()->limit(4)->get();

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        return view('about', ['categorys'=>$cate, 'banners'=>$banner, 'small_banners'=>$small_banner, 'favs'=>$fav, ]);
    }
}

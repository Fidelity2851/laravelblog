<?php

namespace App\Http\Controllers;

use App\banner;
use App\category;
use App\faq;
use App\media;
use App\post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class headercontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth')->except(['login']);
    }

    public function index(){
        if (Auth::check()){
            return redirect('/admin/dashboard');
        }
        else{
            return view('auth.login');
        }
    }

    public function dash(){
        $this->authorize('viewAny', User::class);
        $act = 1;
        if (Gate::allows('isAdmin')) {

            $count1 = post::all()->count();
            $count22 = post::all()->where('status', 1)->count();
            $count33 = post::all()->where('status', 0)->count();
            $count2 = category::all()->count();
            $count3 = User::all()->count();
            $count4 = User::where('role', 'writter')->count();
            $count5 = banner::all()->count();
            $count6 = media::all()->count();
            $count7 = faq::all()->count();

            return view('admin.dashboard', ['dash' => $act, 'post_row'=>$count1, 'post_approved_row'=>$count22, 'post_pending_row'=>$count33, 'cate_row'=>$count2, 'user_row'=>$count3, 'write_row'=>$count4, 'banner_row'=>$count5, 'media_row'=>$count6, 'faq_row'=>$count7]);
        }
        elseif (Gate::allows('isWritter')) {

            $count1 = post::all()->where('user_id', Auth::user()->id)->count();
            $count2 = post::all()->where('user_id', Auth::user()->id)->where('status', 1)->count();
            $count3 = post::all()->where('user_id', Auth::user()->id)->where('status', 0)->count();
            $count4 = User::where('role', 'writter')->where('id', Auth::user()->id)->count();
//            $count5 = banner::all()->count();
//            $count6 = media::all()->count();
//            $count7 = faq::all()->count();

            return view('admin.dashboard', ['dash' => $act, 'post_row'=>$count1, 'post_approved_row'=>$count2, 'post_pending_row'=>$count3, 'write_row'=>$count4, /*'banner_row'=>$count5, 'media_row'=>$count6, 'faq_row'=>$count7*/]);
        }




    }

}

<?php

namespace App\Http\Controllers;

use App\tag;
use Auth;
use App\User;
use App\category;
use App\post;
use App\subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class postcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', post::class);

        $act = 1;

        //$post = post::where('user_id', Auth::User()->id)->paginate(15);
        $post1 = category::all();
        $post2 = subcategory::all();

        return view('admin.post', ['query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
    }

    /*public function indexapprove(){
        $this->authorize('viewAny', post::class);

        $act = 1;
        if (Gate::allows('isAdmin')) {

            $post = post::where('status', '1')->orderby('id', 'desc')->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('status', '1')->where('user_id', Auth::User()->id)->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
    }

    public function indexpending(){
        $this->authorize('viewAny', post::class);

        $act = 1;
        if (Gate::allows('isAdmin')) {

            $post = post::where('status', '0')->orderby('id', 'desc')->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('status', '0')->where('user_id', Auth::User()->id)->paginate(15);
            $post1 = category::all();
            $post2 = subcategory::all();

            return view('admin.post', ['query'=>$post, 'query1'=>$post1, 'query2'=>$post2, 'post' => $act]);
        }
    }*/

    public function store(Request $request){
        $this->authorize('create', post::class);

        \request()->validate([
            'title'=>'required|max:225',
            'slug'=>'max:225',
            'image'=>'required|image|mimes:jpg,png,bmp,wbmp,jpeg,gif,svg',
            'file'=>'file|mimetypes:audio/mpeg,audio/mp4,audio/x-wav,audio/basic,audio/webm, video/quicktime,video/mp4,video/mpeg,video/x-matroska|max:10000',
            'category'=>'required',
            'subcategory'=>'required',
            'summary'=>'required',
            'tags'=>'required',
            'description'=>'required',
            'pub_date'=>'required',
        ]);

        $post = new post();
        $post->title = \request('title');
        $post->slug = \request('slug');
        if ($request->hasFile('image')){
            $post->image = \request('image')->store('uploads', 'public');
        }
        if ($request->hasFile('file')){
            $post->file = \request('file')->storeAs('media', time().$request->file('file')->getClientOriginalName(), 'public');
        }
        $post->category_id = \request('category');
        $post->subcategory_id = \request('subcategory');
        $post->user_id = Auth::user()->id;
        $post->summary = \request('summary');
        $post->description = \request('description');

        //checking whether Admin OR Writter to assign STATUS
        if (Gate::allows('isAdmin')) {
            $post->status = 1;
        }
        elseif (Gate::allows('isWritter')) {
            $post->status = 0;
        }

        $post->pub_at = \request('pub_date');
        $post->save();

        $tags = \request('tags');
        if (!empty($tags)){
            $taglist = explode(',', $tags);
            foreach ($taglist as $tag){
                $post->tag()->create([
                    'name' => $tag,
                ]);

            }
        }

        //image resizing
        $image = Image::make(public_path('storage/' . $post->image))->fit(200, 200, null, 'top');
        $image->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function ajax_sub_cate($id){

        $querys = subcategory::where('category_id',$id)->get();

        return response()->json($querys);
    }

    /*public function approve(post $post){
        $this->authorize('approve', $post);

        post::findorfail($post->id)->update([
            'status' => 1 ,
        ]);

        return back()->with('msg', 'Approved Successfully');
    }

    public function pending(post $post){
        $this->authorize('approve', $post);

        post::findorfail($post->id)->update([
            'status' => 0 ,
        ]);

        return back()->with('msg', 'Pending Successfully');
    }*/

    public function show(post $post){
        $this->authorize('view', $post);

        $act = 1;

        if ($post->has('tag')){
            $tag = $post->tag->pluck('name')->toArray();
            $tags = implode(', ', $tag);
        }
        else{
            $tags = "";
        }
        $query1 = category::all();
        $query2 = subcategory::all();
        return view('admin.editpost', ['post'=>$act, 'query'=>$post, 'query1'=>$query1, 'query2'=>$query2, 'tag'=>$tags]);
    }

    public function update(Request $request, post $post){
        $this->authorize('update', $post);
        $data = \request()->validate([
            'title'=>'required',
            'slug'=>'',
            'image'=>'image|mimes:jpg,png,bmp,wbmp,jpeg,gif,svg',
            'file'=>'file|mimetypes:audio/mpeg,audio/mp4,audio/x-wav,audio/basic,audio/webm, video/quicktime,video/mp4,video/mpeg,video/x-matroska|max:100000',
            'category'=>'required',
            'subcategory'=>'required',
            'summary'=>'required',
            'tags'=>'required',
            'description'=>'required',
            'pub_date'=>'required',
        ]);

        $post->update([
            'title' => \request('title'),
            'slug' => \request('slug'),
            'category_id' => \request('category'),
            'subcategory_id' => \request('subcategory'),
            'summary' => \request('summary'),
            'description' => \request('description'),
            'pub_at' => \request('pub_date')
        ]);

        if ($request->hasFile('image')){
            if ($post->image){
                Storage::delete('/public/' .$post->image);
            }
            $post->update([
                'image' => \request('image')->store('uploads', 'public'),
            ]);
        }
        //image resizing
        $image = Image::make(public_path('storage/' . $post->image))->fit(200, 200, null, 'top');
        $image->save();

        if ($request->hasFile('file')){
            if ($post->file){
                Storage::delete('/public/' .$post->file);
            }
            $post->update([
                'file' => \request('file')->storeAs('media', time().$request->file('file')->getClientOriginalName(), 'public'),
            ]);
        }

        $tags = \request('tags');
        if (!empty($tags)){
            $taglist = explode(',', $tags);
            tag::where('post_id', $post->id)->delete();

            foreach ($taglist as $tag){
                tag::create([
                    'post_id'=>$post->id,
                    'name'=>$tag,
                ]);
            }
        }
        else{
            //
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(post $post){
        $this->authorize('delete', $post);

        //deleting does Image & Musics OR Videos from the upload folder
        if ($post->image){
            Storage::delete('/public/' .$post->image);
        }
        if($post->file){
            Storage::delete('/public/' .$post->file);
        }
        $post->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        $delete = post::whereIn('id', $checked);
        if (!empty($checked)){
            //deleting does image from the upload folder
            foreach ($checked as $check){
                $del = post::find($check);
                if($del->image){
                    Storage::delete('/public/' .$del->image);
                }
                if($del->file){
                    Storage::delete('/public/' .$del->file);
                }
            }
        }
        else{
            dd('some thing went wrong');
        }
        $delete->delete();

        return back()->with('msg', 'Delected Successfully');
    }
}

<?php

namespace App\Http\Controllers;

use App\gallery;
use App\media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Intervention\Image\Facades\Image;

class mediacontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', media::class);

        $act = 1;
        $media = media::orderby('id', 'desc')->paginate(15);

        return view('admin.media', ['med' => $act, 'medias'=>$media]);
    }

    public function store(){
        $this->authorize('create', media::class);

        \request()->validate([
            'title'=>'required',
            'image'=>'required|file|image',
            'photos'=>'file|image',
            'description'=>'required'
        ]);

        $media = new media();
        $media->title = \request('title');
        $media->image = \request('image')->store('uploads', 'public');
        $media->description = \request('description');
        $media->save();

        $image = Image::make(public_path('storage/' . $media->image))->fit(200, 200, null, 'top');
        $image->save();


        return back()->with('msg', 'Created Successfully');
    }

    public function store2(){
        $this->authorize('create', gallery::class);

        \request()->validate([
            'media_title'=>'required',
            'photos'=>'required|file|image',
        ]);

        $gallery = new gallery();
        $gallery->media_id = \request('media_title');
        $gallery->name = \request('photos')->store('uploads', 'public');
        $gallery->save();

        $image = Image::make(public_path('storage/' . $gallery->name))->fit(130, 130, null, 'top');
        $image->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function show(media $media){
        $this->authorize('view', $media);

        $act = 1;
        $query = media::findorfail($media->id);

        return view('admin.editmedia', ['med'=>$act, 'query'=>$query]);
    }

    public function update(media $media){
        $this->authorize('update', $media);

        \request()->validate([
            'title'=>'required',
            'image'=>'file|image',
            'description'=>'required'
        ]);

        if (empty(\request('image'))){
            $media = media::findorfail($media->id)
                ->update([
                    'title' => \request('title'),
                    'description' => \request('description')
                ]);
        }
        else{
            $media = media::findorfail($media->id);
            if ($media->image){
                Storage::delete('/public/' .$media->image);
            }
                $media->update([
                    'title' => \request('title'),
                    'image' => \request('image')->store('uploads', 'public'),
                    'description' => \request('description')
                ]);
            $image = Image::make(public_path('storage/' . $media->image))->fit(200, 200, null, 'top');
            $image->save();
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function update2($gallery){
        $this->authorize('create', gallery::class);

        \request()->validate([
            'media_title'=>'required',
            'photos'=>'required|file|image',
        ]);

        $media = media::findorfail($gallery);
        if (!empty(\request('photos'))){
            $image = $media->gallery()->create([
                'name' => \request('photos')->store('uploads', 'public')
            ]);
            $image = Image::make(public_path('storage/' . $image->name))->fit(130, 130, null, 'top');
            $image->save();
        }

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(media $media){
        $this->authorize('delete', $media);

        $delect = media::findorfail($media->id);
        $delect->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroy2(gallery $gallery){
        $this->authorize('delete', $gallery);

        $delect = gallery::findorfail($gallery->id);
        $delect->delete();

        return back()->with('msg', 'Deleted Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        media::whereIn('id', $checked)->delete();

        return back()->with('msg', 'Deleted Successfully');
    }
}

<?php

namespace App\Http\Controllers;

use App\category;
use App\post;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(){
        /*Header category*/
        $cate = category::with('subcategory')->get();

        /*Banner posts*/
        $banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(3)->get();
        $small_banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->inRandomOrder()->limit(4)->get();

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        /*Homepage posts*/
        $categorys = category::all();
        $i = 1;

        foreach ($categorys as $category){

            $new[$i] = post::where('category_id', $category->id)->where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('id', 'desc')->limit(6)->get();

            $i++;
        }
        if (empty($new[1])){
            $new[1] = [];
        }
        if (empty($new[2])){
            $new[2] = [];
        }
        if (empty($new[3])){
            $new[3] = [];
        }
        if (empty($new[4])){
            $new[4] = [];
        }
        if (empty($new[5])){
            $new[5] = [];
        }

        return view('index', ['categorys'=>$cate, 'banners'=>$banner, 'small_banners'=>$small_banner, 'musics'=>$new[1], 'videos'=>$new[2], 'news'=>$new[3], 'stories'=>$new[4], 'status'=>$new[5], 'favs'=>$fav, ]);

    }
}

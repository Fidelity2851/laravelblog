<?php

namespace App\Http\Controllers;

use App\setting;
use Illuminate\Http\Request;

class settingscontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public  function index(){
        $this->authorize('create', setting::class);

        $act = 1;
        $setting = setting::firstOrcreate(
            ['id' => 1]
        );

        return view('admin.settings', ['set'=>$act, 'setting'=>$setting]);
    }

    public function update(){
        $this->authorize('create', setting::class);

        \request()->validate([
            'facebook'=>'sometimes',
            'twitter'=>'sometimes',
            'instagram'=>'sometimes',
            'google'=>'sometimes'
        ]);


        setting::first()->update([
           'facebook' => \request('facebook'),
            'twitter' => \request('twitter'),
            'instagram' => \request('instagram'),
            'google' => \request('google')
        ]);


        return back()->with('msg', 'Created Successfully');
    }

}

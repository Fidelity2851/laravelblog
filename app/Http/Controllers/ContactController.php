<?php

namespace App\Http\Controllers;

use App\category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index(){
        /*Header category*/
        $cate = category::with('subcategory')->get();

        return view('contact', ['categorys'=>$cate]);
    }
}

<?php

namespace App\Http\Controllers;

use App\category;
use App\post;
use App\subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class categorycontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', category::class);
        $act = 1;
        $query = category::orderby('id', 'desc')->get();
        return view('admin.category', ['query'=>$query, 'cate' => $act]);
    }

    public function store(){
        $this->authorize('create', category::class);

        \request()->validate([
           'category_name'=>'required',
           'description'=>'required'
        ]);

        $msg = "successfully";
        $cate = new category();
        $cate->name = \request('category_name');
        $cate->description = \request('description');
        $cate->save();

        return back()->with('msg', 'Created Successfully');

    }

    public function store2(){
        $this->authorize('create', category::class);

        \request()->validate([
            'category'=>'required',
            'subcategory'=>'required',
            'subdescription'=>'required'
        ]);

        $cate = new subcategory();
        $cate->category_id = \request('category');
        $cate->name = \request('subcategory');
        $cate->description = \request('subdescription');
        $cate->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function show(category $category){
        $this->authorize('view', $category);

        $act = 1;
        $query = category::findorfail($category->id);

        return view('admin.editcategory', ['cate'=>$act, 'query'=>$query]);
    }

    public function show2(subcategory $subcategory){
        $this->authorize('view', $subcategory);

        $act = 1;
        $query = subcategory::where('id', $subcategory->id)->get();
        return view('admin.editsubcategory', ['cate'=>$act, 'query'=>$query]);
    }

    public function update(category $category){
        $this->authorize('update', $category);

        \request()->validate([
            'category_name'=>'required',
            'description'=>'required',
        ]);

        category::where('id', $category->id)
            ->update([
                'name' => \request('category_name'),
                'description' => \request('description')
            ]);;

        return back()->with('msg', 'Updated Successfully');
    }

    public function update2(subcategory $subcategory){
        $this->authorize('update', $subcategory);

        \request()->validate([
            'subcategory'=>'required',
            'description'=>'required',
        ]);

        subcategory::where('id', $subcategory->id)
            ->update([
                'name' => \request('subcategory'),
                'description' => \request('description')
            ]);;

        return back()->with('msg', 'Updated Successfully');
    }

    public function destroy(category $category){
        $this->authorize('delete', $category);

        $delete = category::findorfail($category->id);
        $delete->delete();

        return back()->with('msg', 'Delected Successfully');
    }

    public function destroy2(subcategory $subcategory){
        $this->authorize('delete', $subcategory);

        $delete = subcategory::findorfail($subcategory->id);
        $delete->delete();

        return back()->with('msg', 'Deleted Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        category::whereIn('id', $checked)->delete();

        return back()->with('msg', 'Deleted Successfully');
    }

}


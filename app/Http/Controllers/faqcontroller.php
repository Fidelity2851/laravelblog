<?php

namespace App\Http\Controllers;

use App\faq;
use Illuminate\Http\Request;

class faqcontroller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $this->authorize('viewAny', faq::class);

        $act = 1;
        $faq = faq::orderby('id', 'desc')->paginate(15);

        return view('admin.faq', ['faq'=>$act, 'faqs'=>$faq]);
    }

    public function store(){
        $this->authorize('create', faq::class);

        \request()->validate([
            'question'=>'required',
            'answer'=>'required',
        ]);

        $faq = new faq();
        $faq->questions = \request('question');
        $faq->answers = \request('answer');
        $faq->save();

        return back()->with('msg', 'Created Successfully');
    }

    public function show(faq $faq){
        $this->authorize('view', $faq);

        $act = 1;
        $query = faq::findorfail($faq->id);
        return view('admin.editfaq', ['faq'=>$act,'query'=>$query]);
    }

    public function update(faq $faq){
        $this->authorize('update', $faq);

        \request()->validate([
            'question'=>'required',
            'answer'=>'required',
        ]);

        faq::findorfail($faq->id)
            ->update([
                'questions' => \request('question'),
                'answers' => \request('answer')
            ]);

        return back()->with('msg', 'Updated Successfully');
}

    public function destroy(faq $faq){
        $this->authorize('delete', $faq);

        $delect = faq::findorfail($faq->id);
        $delect->delete();

        return back()->with('msg', 'Deleted Successfully');
    }

    public function destroymany(Request $request){
        \request()->validate([
            'checked'=>'',
        ]);
        $checked = $request->input('checked');
        faq::whereIn('id', $checked)->delete();

        return back()->with('msg', 'Deleted Successfully');
    }
}

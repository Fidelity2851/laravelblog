<?php

namespace App\Http\Controllers;

use App\tag;
use Auth;
use App\category;
use App\comment;
use App\post;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only('create_comment');
    }

    public function category_show($id){
        $cate = category::with('subcategory')->get();

        $post = post::where('category_id', $id)->where('status', 1)->orderby('id', 'desc')->paginate(10);

        /*Banner posts*/
        $banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(3)->get();
        $small_banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->inRandomOrder()->limit(4)->get();

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        return view('category', ['categorys'=>$cate, 'posts'=>$post, 'banners'=>$banner, 'small_banners'=>$small_banner, 'favs'=>$fav]);
    }

    public function subcategory_show($id){
        $cate = category::with('subcategory')->get();

        $post = post::where('subcategory_id', $id)->where('status', 1)->orderby('id', 'desc')->paginate(10);

        /*Banner posts*/
        $banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(3)->get();
        $small_banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->inRandomOrder()->limit(4)->get();

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        return view('category', ['categorys'=>$cate, 'posts'=>$post, 'banners'=>$banner, 'small_banners'=>$small_banner, 'favs'=>$fav]);
    }

    public function tags_show($id){
        /*Header category*/
        $cate = category::with('subcategory')->get();

        /*Banner posts*/
        $banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(3)->get();
        $small_banner = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->inRandomOrder()->limit(4)->get();

        /*Category posts*/
        $post = post::where('id', $id)->paginate(10);

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        return view('category', ['categorys'=>$cate, 'posts'=>$post, 'banners'=>$banner, 'small_banners'=>$small_banner, 'favs'=>$fav]);
    }

    public function show($id){
        /*Header category*/
        $cate = category::with('subcategory')->get();

        /*trending post*/
        $fav = post::where('status', 1)->where('pub_at', '<=', date('y:m:d'))->orderby('views', 'desc')->limit(10)->get();

        $post_view = post::where('id', $id)->increment('views');
        if ($post_view){
            $post = post::findorfail($id);
        }

        return view('reading', ['categorys'=>$cate, 'posts'=>$post, 'favs'=>$fav]);
    }

    public function create_comment($id){
        \request()->validate([
            'comment'=>'required',
        ]);

        $comment = new comment();
        $comment->user_id = Auth::user()->id;
        $comment->post_id = $id;
        $comment->name = \request('comment');
        $comment->save();

        return back()->with('msg', 'Created Successfully');
    }

}

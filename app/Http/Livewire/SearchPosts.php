<?php

namespace App\Http\Livewire;

use App\post;
use Composer\DependencyResolver\Request;
use Livewire\Component;
use Livewire\WithPagination;

class SearchPosts extends Component
{
    use WithPagination;

    public $search;
    public $posts;

    protected $casts = [

    ];

    protected $updatesQueryString = [

    ];

    public function mount(){

    }

    public function searching(){
        $this->validate([
            'search' => ''
        ]);
        $this->posts = post::where('status', 1)->where('title', 'like', '%'. $this->search .'%')->paginate(10);

    }

    public function save(){

    }

    public function updatingSearch(){
        $this->resetPage();
    }

    public function render(){
        return view('livewire.search-posts');
    }
}

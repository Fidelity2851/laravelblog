<?php

namespace App\Http\Livewire;

use App\post;
use Livewire\Component;

class ShowSearchedPost extends Component
{
    public $posts;

    protected $casts = [
      'posts' => 'collection'
    ];

    public function mount($posts){
        $this->posts = $posts;
    }

    public function render()
    {
        return view('livewire.show-searched-post');
    }
}

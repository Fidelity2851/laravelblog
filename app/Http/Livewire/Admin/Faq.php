<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Faq extends Component
{
    public function render()
    {
        return view('livewire.admin.faq');
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Banner extends Component
{
    public $search;

    public function render()
    {
        $banner = \App\banner::paginate(15);

        return view('livewire.admin.banner', ['banners'=>$banner]);
    }
}

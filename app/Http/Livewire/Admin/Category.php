<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Category extends Component
{

    public $search;

    public function mount(){

    }

    public function doreset(){
        $this->search = '';
    }

    public function render()
    {
        $category = \App\category::where('name', 'like', '%'.$this->search.'%')->orderby('id', 'desc')->paginate(15);
        return view('livewire.admin.category', ['query'=>$category]);
    }
}

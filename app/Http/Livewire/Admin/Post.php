<?php

namespace App\Http\Livewire\Admin;

use Auth;
use App\category;
use App\subcategory;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Gate;

class Post extends Component
{
    use WithPagination;

    public $search;
    public $raw = [0,1];

    public function mount(){

    }

    public function doreset(){
        $this->search = '';
    }

    public function all_post(){
        $this->raw = [0,1];

    }
    public function approved_post(){
        $this->raw = [1];

    }
    public function pending_post(){
        $this->raw = [0];

    }


    public function approve($id){

        \App\post::findorfail($id)->update([
            'status' => 1 ,
        ]);

    }
    public function pending($id){

        \App\post::findorfail($id)->update([
            'status' => 0 ,
        ]);

    }

    public function render()
    {
        if (Gate::allows('isAdmin')) {

            $post = \App\post::whereIn('status', $this->raw)->where('title', 'like', '%'. $this->search .'%')->orderby('id', 'desc')->paginate(15);

        }
        elseif (Gate::allows('isWritter')) {

            $post = \App\post::where('title', 'like', '%'. $this->search .'%')->where('user_id', Auth::User()->id)->paginate(15);

        }

        return view('livewire.admin.post', ['query'=>$post]);
    }
}

<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;

class Media extends Component
{
    public function render()
    {
        return view('livewire.admin.media');
    }
}

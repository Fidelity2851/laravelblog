<?php

namespace App\Http\Livewire\Admin;

use App\category;
use App\post;
use App\subcategory;
use Livewire\Component;
use Livewire\WithPagination;

class SearchPost extends Component
{
    use WithPagination;

    public $search;


    protected $updatesQueryString = [

    ];

    public function mount(){

    }

    public function doreset(){
        $this->search = '';
    }

    public function updatingSearch(){

    }

    public function render()
    {
        if (Gate::allows('isAdmin')) {

            $post = post::where('title', 'like', '%'. $this->search .'%')->orderby('id', 'desc')->paginate(15);

        }
        elseif (Gate::allows('isWritter')) {

            $post = post::where('title', 'like', '%'. $this->search .'%')->where('user_id', Auth::User()->id)->paginate(15);

        }

        return view('livewire.admin.search-post',['query' => $post]);
    }
}

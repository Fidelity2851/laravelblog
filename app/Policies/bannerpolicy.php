<?php

namespace App\Policies;

use App\User;
use App\banner;
use Illuminate\Auth\Access\HandlesAuthorization;

class bannerpolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any banners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the banner.
     *
     * @param  \App\User  $user
     * @param  \App\banner  $banner
     * @return mixed
     */
    public function view(User $user, banner $banner)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can create banners.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
           'admin',
        ]);
    }

    /**
     * Determine whether the user can update the banner.
     *
     * @param  \App\User  $user
     * @param  \App\banner  $banner
     * @return mixed
     */
    public function update(User $user, banner $banner)
    {
        return in_array($user->role, [
            'admin',
        ]);
    }

    /**
     * Determine whether the user can delete the banner.
     *
     * @param  \App\User  $user
     * @param  \App\banner  $banner
     * @return mixed
     */
    public function delete(User $user, banner $banner)
    {
        return in_array($user->role, [
            'admin',
        ]);
    }

    /**
     * Determine whether the user can restore the banner.
     *
     * @param  \App\User  $user
     * @param  \App\banner  $banner
     * @return mixed
     */
    public function restore(User $user, banner $banner)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the banner.
     *
     * @param  \App\User  $user
     * @param  \App\banner  $banner
     * @return mixed
     */
    public function forceDelete(User $user, banner $banner)
    {
        //
    }
}

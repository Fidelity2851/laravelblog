<?php

namespace App\Policies;

use App\User;
use App\gallery;
use Illuminate\Auth\Access\HandlesAuthorization;

class gallerypolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any galleries.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the gallery.
     *
     * @param  \App\User  $user
     * @param  \App\gallery  $gallery
     * @return mixed
     */
    public function view(User $user, gallery $gallery)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can create galleries.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can update the gallery.
     *
     * @param  \App\User  $user
     * @param  \App\gallery  $gallery
     * @return mixed
     */
    public function update(User $user, gallery $gallery)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can delete the gallery.
     *
     * @param  \App\User  $user
     * @param  \App\gallery  $gallery
     * @return mixed
     */
    public function delete(User $user, gallery $gallery)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can restore the gallery.
     *
     * @param  \App\User  $user
     * @param  \App\gallery  $gallery
     * @return mixed
     */
    public function restore(User $user, gallery $gallery)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the gallery.
     *
     * @param  \App\User  $user
     * @param  \App\gallery  $gallery
     * @return mixed
     */
    public function forceDelete(User $user, gallery $gallery)
    {
        //
    }
}

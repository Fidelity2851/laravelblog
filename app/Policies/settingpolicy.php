<?php

namespace App\Policies;

use App\User;
use App\setting;
use Illuminate\Auth\Access\HandlesAuthorization;

class settingpolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any settings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can view the setting.
     *
     * @param  \App\User  $user
     * @param  \App\setting  $setting
     * @return mixed
     */
    public function view(User $user, setting $setting)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can create settings.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can update the setting.
     *
     * @param  \App\User  $user
     * @param  \App\setting  $setting
     * @return mixed
     */
    public function update(User $user, setting $setting)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can delete the setting.
     *
     * @param  \App\User  $user
     * @param  \App\setting  $setting
     * @return mixed
     */
    public function delete(User $user, setting $setting)
    {
        return in_array($user->role, [
            'admin'
        ]);
    }

    /**
     * Determine whether the user can restore the setting.
     *
     * @param  \App\User  $user
     * @param  \App\setting  $setting
     * @return mixed
     */
    public function restore(User $user, setting $setting)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the setting.
     *
     * @param  \App\User  $user
     * @param  \App\setting  $setting
     * @return mixed
     */
    public function forceDelete(User $user, setting $setting)
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subcategory extends Model
{

    protected $guarded = [];


    public function category(){
        return $this->belongsTo(category::class);
    }

    public function post(){
        return $this->hasMany(post::class);
    }
}

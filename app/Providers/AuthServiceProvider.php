<?php

namespace App\Providers;

use App\banner;
use App\Policies\bannerpolicy;
use App\post;
use App\Policies\postpolicy;
use App\category;
use App\policies\categorypolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\model' => 'App\Policies\modelPolicy',
        category::class => categorypolicy::class,
        post::class => postpolicy::class,
        banner::class => bannerpolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /* define a admin user role */
        Gate::define('isAdmin', function($user) {
            return $user->role == 'admin';
        });

        /* define a manager user role */
        Gate::define('isWritter', function($user) {
            return $user->role == 'writter';
        });

        /* define a user role */
        Gate::define('isUser', function($user) {
            return $user->role == 'user';
        });
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*Frontend Route*/

/*Home page Routes*/
Route::get('/', 'homecontroller@index')->name('home');

/*Posts page Routes*/
Route::get('/posts/category/{id}', 'maincontroller@category_show')->name('posts.category');
Route::get('/posts/subcategory/{id}', 'maincontroller@subcategory_show')->name('posts.subcategory');
Route::get('/posts/tag/{id}', 'maincontroller@tags_show')->name('posts.tag');
Route::get('/post/{id}', 'maincontroller@show')->name('read');
Route::post('/post/comment/{id}', 'maincontroller@create_comment')->name('create.comment');

/*About page Routes*/
Route::get('/about', 'aboutcontroller@index')->name('about');

/*Contact page Routes*/
Route::get('/contact', 'contactcontroller@index')->name('contact');


/*Frontend Route ENDS*/


/*Backend Route*/

/*Login & Home page Route*/
Route::get('/admin', 'headercontroller@index')->name('admin');
Route::get('/admin/dashboard', 'headercontroller@dash')->name('dashboard');


/*Admin Category Routes*/
Route::get('/admin/category', 'categorycontroller@index')->name('category.list');
Route::post('/admin/create/category', 'categorycontroller@store')->name('category.store');
Route::post('/admin/create/subcategory', 'categorycontroller@store2')->name('subcategory.store');
Route::get('/admin/category/{category}', 'categorycontroller@show')->name('category.fetch');
Route::get('/admin/subcategory/{subcategory}', 'categorycontroller@show2')->name('subcategory.fetch');
Route::post('/admin/update/category/{category}', 'categorycontroller@update')->name('category.update');
Route::post('/admin/update/subcategory/{subcategory}', 'categorycontroller@update2')->name('subcategory.update');
Route::get('/admin/delete/category/{category}', 'categorycontroller@destroy')->name('category.delete');
Route::post('/admin/delete/category', 'categorycontroller@destroymany')->name('category.deletemany');
Route::get('/admin/delete/subcategory/{subcategory}', 'categorycontroller@destroy2')->name('subcategory.delete');

/*Admin Post Routes*/
Route::get('/admin/posts', 'postcontroller@index')->name('post.list');
Route::get('/admin/posts/approve', 'postcontroller@indexapprove')->name('post.listapprove');
Route::get('/admin/posts/pending', 'postcontroller@indexpending')->name('post.listpending');
Route::post('/admin/create/post', 'postcontroller@store')->name('post.store');
Route::get('/admin/posts/{post}', 'postcontroller@show')->name('post.fetch');
Route::get('/admin/posts/ajax/{id}', 'postcontroller@ajax_sub_cate');
Route::post('/admin/update/post/{post}', 'postcontroller@update')->name('post.update');
Route::get('/admin/approve/posts/{post}', 'postcontroller@approve')->name('post.approve');
Route::get('/admin/pending/posts/{post}', 'postcontroller@pending')->name('post.pending');
Route::get('/admin/delete/posts/{post}', 'postcontroller@destroy')->name('post.delete');
Route::post('/admin/delete/posts', 'postcontroller@destroymany')->name('post.deletemany');


/*Admin Banner Routes*/
Route::get('/admin/banner', 'bannercontroller@index')->name('banner.list');
Route::post('/admin/create/banner', 'bannercontroller@store')->name('banner.store');
Route::get('/admin/banner/{banner}', 'bannercontroller@show')->name('banner.fetch');
Route::post('/admin/update/banner/{banner}', 'bannercontroller@update')->name('banner.update');
Route::get('/admin/delete/banner/{banner}', 'bannercontroller@destroy')->name('banner.delete');
Route::post('/admin/delete/banner}', 'bannercontroller@destroymany')->name('banner.deletemany');


/*Admin Media Routes*/
Route::get('/admin/media', 'mediacontroller@index')->name('media.list');
Route::post('/admin/create/media', 'mediacontroller@store')->name('media.store');
Route::post('/admin/create/gallery', 'mediacontroller@store2')->name('gallery.store');
Route::get('/admin/media/{media}', 'mediacontroller@show')->name('media.fetch');
Route::post('/admin/update/media/{media}', 'mediacontroller@update')->name('media.update');
Route::post('/admin/update/gallery/{gallery}', 'mediacontroller@update2')->name('gallery.update');
Route::get('/admin/delete/media/{media}', 'mediacontroller@destroy')->name('media.delete');
Route::post('/admin/delete/media', 'mediacontroller@destroymany')->name('media.deletemany');
Route::get('/admin/delete/gallery/{gallery}', 'mediacontroller@destroy2')->name('gallery.delete');


/*Admin Faq Routes*/
Route::get('/admin/faq', 'faqcontroller@index')->name('faq.list');
Route::post('/admin/create/faq', 'faqcontroller@store')->name('faq.store');
Route::get('/admin/faq/{faq}', 'faqcontroller@show')->name('faq.fetch');
Route::post('/admin/update/faq/{faq}', 'faqcontroller@update')->name('faq.update');
Route::get('/admin/delete/faq/{faq}', 'faqcontroller@destroy')->name('faq.delete');
Route::post('/admin/delete/faq', 'faqcontroller@destroymany')->name('faq.deletemany');


/*Admin Users Routes*/
Route::get('/admin/users', 'userscontroller@index')->name('users.list');
Route::post('/admin/create/users', 'userscontroller@store')->name('users.store');
Route::get('/admin/users/{user}', 'userscontroller@show')->name('users.fetch');
Route::post('/admin/update/user/{user}', 'userscontroller@update')->name('users.update');
Route::get('/admin/block/users/{user}', 'userscontroller@block')->name('users.block');
Route::get('/admin/unblock/users/{user}', 'userscontroller@unblock')->name('users.unblock');
Route::get('/admin/delete/users/{user}', 'userscontroller@destroy')->name('users.delete');
Route::post('/admin/delete/users', 'userscontroller@destroymany')->name('users.deletemany');


/*Admin Settings Routes*/
Route::get('/admin/setting', 'settingscontroller@index')->name('setting.list');
Route::post('/admin/create/setting', 'settingscontroller@update')->name('setting.update');

/*Backend Route ENDS*/

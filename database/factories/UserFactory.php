<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(\App\post::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'category_id' => 1,
        'subcategory_id' => 1,
        'user_id' => 4,
        'summary' => $faker->text,
        'description' => $faker->text,
        'status' => 1,
        'pub_at' => $faker->dateTime,
        'image' => 'uploads/i5gbRDx0ws6nECjiHJIL2XCSmp5rl8LtcUvWYjCu.jpeg',
    ];
});



<!--dashboard bar-->


<!--title-->
<?php $__env->startSection('title'); ?>
    Users
<?php $__env->stopSection(); ?>

<!--display-->
<?php $__env->startSection('content'); ?>
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">users</p>
            <div class="flex-self-center">
                <p class="dis_bind_act">users</p>
            </div>
        </div>

        <div class="post_con p-4">
            <?php if(session()->has('msg')): ?>
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            <?php endif; ?>
            <ul data-role="tabs" data-expand="true">
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', App\User::class)): ?>
                    <li><a href="#_target_1" class="tab_link">Create Users</a></li>
                <?php endif; ?>
                <li><a href="#_target_2" class="tab_link">Manger Users</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', App\User::class)): ?>
                    <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="<?php echo e(route('users.store')); ?>" method="post" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Username</label>
                                        <input type="text" value="<?php echo e(old('username')); ?>" name="username" class="post_box  <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                        <?php $__errorArgs = ['username'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Email Address</label>
                                        <input type="Email" value="<?php echo e(old('email')); ?>" name="email" class="post_box  <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">User Role</label>
                                        <select name="role" class="post_sel  <?php $__errorArgs = ['role'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="select" required>
                                            <option value="user">User</option>
                                            <option value="writter">Writter</option>
                                            <option value="admin">Admin</option>
                                        </select>
                                        <?php $__errorArgs = ['role'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                </div>
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Profile photo</label>
                                        <input type="file" name="image" class="post_box  <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="file" required>
                                        <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Password</label>
                                        <input type="password" name="password" class="post_box  <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Comfirm Password</label>
                                        <input id="password-confirm" type="password" name="comfirm_password" class="post_box  <?php $__errorArgs = ['comfirm_password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                        <?php $__errorArgs = ['comfirm_password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Short Biograph</label>
                                    <textarea name="biograph" class="post_area <?php $__errorArgs = ['biograph'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="textarea" required><?php echo e(old('biograph')); ?></textarea>
                                    <?php $__errorArgs = ['biograph'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="col ">
                                    <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div id="_target_2">
                    <form method="post" action="<?php echo e(route('users.deletemany')); ?>">
                        <?php echo csrf_field(); ?>
                        <table class="table table-border row-border compact striped"
                               data-check="false"
                               data-show-search="false"
                               data-show-rows-steps="false"
                               data-horizontal-scroll="false"
                               data-show-pagination="false"
                        >
                            <thead>
                            <tr>
                                <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                                <th class="t_head" data-sortable="true">ID</th>
                                <th class="t_head" data-sortable="true">Username</th>
                                <th class="t_head" data-sortable="true">Biograph</th>
                                <th class="t_head" data-sortable="true">Email</th>
                                <th class="t_head" data-sortable="true">Role</th>
                                <th class="t_head" data-sortable="true">Status</th>
                                <th class="t_head" data-sortable="true">Registered date</th>
                                <th class="t_head">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td class="t_data"><input value="<?php echo e($user->id); ?>" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                    <td class="t_data"><?php echo e($user->id); ?></td>
                                    <td class="t_data"><img class="dash_img mr-3" src="storage/<?php echo e($user->image); ?>"> <?php echo e($user->username); ?></td>
                                    <td class="t_data"><?php echo e($user->biograph); ?></td>
                                    <td class="t_data"><?php echo e($user->email); ?></td>
                                    <td class="t_data">
                                        <span class="data_w"><?php echo e($user->role); ?></span>
                                    </td>
                                    <td class="t_data">
                                        <?php if($user->status==1): ?>
                                            <span class="data_g">Active</span>
                                        <?php elseif($user->status==0): ?>
                                            <span class="data_r">Blocked</span>
                                        <?php endif; ?>
                                    </td>
                                    <td class="t_data"><?php echo e($user->created_at); ?></td>
                                    <td>
                                        <?php if($user->status==0): ?>
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', App\User::class)): ?>
                                                <a href="<?php echo e(route('users.unblock', ['user' => $user->id])); ?>" title="Unblock" class="mif-done t_icon unblo_btn"></a>
                                            <?php endif; ?>
                                        <?php elseif($user->status==1): ?>
                                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('create', App\User::class)): ?>
                                                <a href="<?php echo e(route('users.block', ['user' => $user->id])); ?>" title="Block" class="mif-lock t_icon blo_btn"></a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <a href="<?php echo e(route('users.fetch', ['user' => $user->id])); ?>" title="Edit" class="mif-open-book t_icon">

                                        </a> <a href="<?php echo e(route('users.delete', ['user' => $user->id])); ?>" title="Delete" class="mif-bin t_icon con_del"></a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                        <div class="">
                            <button id="del_btn" type="submit" class="del_all_btn mb-3">Delete</button>
                            <?php echo e($users->links()); ?>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\CMS\resources\views/users.blade.php ENDPATH**/ ?>
<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <title>CMS - <?php echo $__env->yieldContent('title'); ?></title>

    <!--css-->
    <link rel="stylesheet" href="<?php echo e(asset('css/index.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/metro-all.css')); ?>">



    <script src="https://cdn.ckeditor.com/ckeditor5/19.1.1/classic/ckeditor.js"></script>

    <!--google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Buda:300|Muli:300,400,500,600,700|Raleway:300,400,500,600,700&display=swap" rel="stylesheet">

    <!--fonts icon-->


</head>
<body>
<!--housing-->
<div  class="house d-flex flex-justify-between">

    <!--dashboard bar-->
    <div class="col-2 dash_con pos-fixed p-0">
        <div class="col px-4">
            <p class="dis_header text-center">cms</p>
        </div>
        <div class="col dash_img_con d-flex py-4">
            <?php if(Auth::check()): ?>
                <?php if(Auth::user()->image): ?>
                    <img class="dash_img flex-align-center mr-3" src="<?php echo e(asset('../storage/'. Auth::user()->image)); ?>" alt="<?php echo e(Auth::user()->image); ?>"/>
                <?php else: ?>
                    <img class="dash_img flex-align-center mr-3" src="<?php echo e(asset("../images/user_icon.svg")); ?>" alt="User image"/>
                <?php endif; ?>
                <div class="flex-self-center">
                    <p class="dash_img_name"><?php echo e(Auth::user()->username); ?></p>
                    <div class="d-flex">
                        <p class="log_role flex-self-center"><?php echo e(Auth::user()->role); ?></p>
                        <a class="dropdown-item no-decor flex-self-center ml-3" href="<?php echo e(route('logout')); ?>"
                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                            <span class="log"><?php echo e(__('Logout')); ?></span>
                        </a>
                    </div>

                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                        <?php echo csrf_field(); ?>
                    </form>
                </div>
            <?php endif; ?>
        </div>
        <div class="dash_link_con">
            <a href="<?php echo e(route('dashboard')); ?>" class="no-decor"> <p class="<?php if(isset($dash)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">dashboard</p> </a>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\category::class)): ?>
                <a href="<?php echo e(route('category.list')); ?>" class="no-decor"> <p class="<?php if(isset($cate)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">category</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\post::class)): ?>
                <a href="<?php echo e(route('post.list')); ?>" class="no-decor"> <p class="<?php if(isset($post)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">post</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\banner::class)): ?>
                <a href="<?php echo e(route('banner.list')); ?>" class="no-decor"> <p class="<?php if(isset($ban)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">banner</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\media::class)): ?>
                <a href="<?php echo e(route('media.list')); ?>" class="no-decor"> <p class="<?php if(isset($med)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">multimedia</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\faq::class)): ?>
                <a href="<?php echo e(route('faq.list')); ?>" class="no-decor"> <p class="<?php if(isset($faq)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">faq</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\User::class)): ?>
                <a href="<?php echo e(route('users.list')); ?>" class="no-decor"> <p class="<?php if(isset($user)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">users</p> </a>
            <?php endif; ?>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewAny', App\setting::class)): ?>
                <a href="<?php echo e(route('setting.list')); ?>" class="no-decor"> <p class="<?php if(isset($set)): ?> dash_link_act  <?php else: ?> dash_link <?php endif; ?>">setting</p> </a>
            <?php endif; ?>
        </div>
    </div>

    <?php echo $__env->yieldContent('content'); ?>


</div>


<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .then( editor => {
            console.log( editor );
        } )
        .catch( error => {
            console.error( error );
        } );

</script>


<script type="text/javascript" src="<?php echo e(asset('js/jquery-3.4.1.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/metro.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/index.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\laragon\www\CMS\resources\views/layout.blade.php ENDPATH**/ ?>

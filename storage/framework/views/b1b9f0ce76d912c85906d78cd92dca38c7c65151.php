<!--dashboard bar-->


<!--title-->
<?php $__env->startSection('title'); ?>
    Media
<?php $__env->stopSection(); ?>

<!--display-->
<?php $__env->startSection('content'); ?>
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">media</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">Media</p>
                </div>
            </div>


            <div class="post_con p-4">
                <?php if(session()->has('msg')): ?>
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                <?php endif; ?>

                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Media</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Media</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                            <div class="col-5 p-0">
                                <form class="post_form" action="<?php echo e(route('media.store')); ?>" method="post" enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <div class="col ">
                                        <label class="post_label">Media Title</label>
                                        <input type="text" value="<?php echo e(old('title')); ?>" name="title" class="post_box <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                        <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Cover Image</label>
                                        <input type="file" name="image" class="post_box <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="file" required>
                                        <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Description</label>
                                        <textarea name="description" class="post_area <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="textarea" required><?php echo e(old('description')); ?></textarea>
                                        <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                        <button type="submit" class="post_btn2">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-5 p-0">
                                <form class="post_form" action="<?php echo e(route('gallery.store')); ?>" method="post" enctype="multipart/form-data">
                                    <?php echo csrf_field(); ?>
                                    <div class="col ">
                                        <label class="post_label">Media Title</label>
                                        <select name="media_title" class="post_sel <?php $__errorArgs = ['media_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="select" required>
                                            <option disabled selected>Select a Media Title</option>
                                            <?php $__currentLoopData = $medias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($data->id); ?>"><?php echo e($data->title); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                        <?php $__errorArgs = ['media_title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Photos (Mulit select)</label>
                                        <input type="file" name="photos" class="post_box <?php $__errorArgs = ['photos'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="file" multiple required>
                                        <?php $__errorArgs = ['photos'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                        <p class="error_msg"><?php echo e($message); ?></p>
                                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                    </div>
                                    <div class="col ">
                                        <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                        <button type="submit" class="post_btn2">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="_target_2">
                        <form method="post" action="<?php echo e(route('media.deletemany')); ?>">
                            <?php echo csrf_field(); ?>
                            <table class="table table-border row-border compact striped"
                                   data-check="false"
                                   data-show-search="false"
                                   data-show-rows-steps="false"
                                   data-horizontal-scroll="false"
                                   data-show-pagination="false"
                            >
                                <thead>
                                <tr>
                                    <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                                    <th class="t_head" data-sortable="true">ID</th>
                                    <th class="t_head" data-sortable="true">Title</th>
                                    <th class="t_head" data-sortable="true">Cover Image</th>
                                    <th class="t_head" data-sortable="true">Description</th>
                                    <th class="t_head" data-sortable="true">Created date</th>
                                    <th class="t_head">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $medias; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $media): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="t_data"><input value="<?php echo e($media->id); ?>" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                        <td><?php echo e($media->id); ?></td>
                                        <td><?php echo e($media->title); ?></td>
                                        <td><img class="post_img" src="<?php echo e(asset('storage/'.$media->image)); ?>"></td>
                                        <td><?php echo e($media->description); ?></td>
                                        <td><?php echo e($media->created_at); ?></td>
                                        <td>
                                            <a href="<?php echo e(route('media.fetch', ['media' => $media->id])); ?>" title="Edit" class="mif-open-book t_icon"></a>
                                            <a href="<?php echo e(route('media.delete', ['media' => $media->id])); ?> " title="Delete" class="mif-bin t_icon con_del"></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="">
                                <button type="submit" class="del_all_btn mb-3">Delete</button>
                                <?php echo e($medias->links()); ?>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\CMS\resources\views/media.blade.php ENDPATH**/ ?>
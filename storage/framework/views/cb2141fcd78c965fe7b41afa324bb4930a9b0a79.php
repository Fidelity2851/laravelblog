

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CMS - Login</title>

        <!--css-->
        <link rel="stylesheet" href="css/index.css">
        <link rel="stylesheet" href="css/metro.css">

        <!--google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Buda:300|Muli:300,400,500,600,700|Raleway:300,400,500,600,700&display=swap" rel="stylesheet">

        <!--fonts icon-->
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    </head>
    <body>
    <!--housing-->
    <div class="house1 d-flex flex-justify-center">
        <?php if(session()->has('msg')): ?>
            <div class="msg_con mt-5">
                <div class="msg d-flex flex-justify-between flex-self-start">
                    <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                    <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                    <button type="button" class="msg_btn flex-self-center">&times;</button>
                </div>
            </div>
        <?php endif; ?>

        <div class="d-flex flex-justify-center flex-self-center px-10">
            <!--login-->
            <div class="log_con col-3 flex-self-center p-10 mr-10">
                <p class="form_header">login</p>
                <form class="log_form" method="POST" action="<?php echo e(route('login')); ?>">
                    <?php echo csrf_field(); ?>

                    <div class="mb-4">
                        <label class="log_label"><?php echo e(__('E-Mail Address')); ?></label>
                        <input id="email" type="email" name="email" class="log_box <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" value="<?php echo e(old('email')); ?>" data-role="input" required autocomplete="email" autofocus>
                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback log_label" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <div class="mb-4">
                        <label class="log_label"><?php echo e(__('Password')); ?></label>
                        <input id="password" type="password" class="log_box <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" name="password" required autocomplete="current-password">
                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                        <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </div>
                    <div class="mb-2">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

                        <label class="log_label" for="remember">
                            <?php echo e(__('Remember Me')); ?>

                        </label>
                    </div>
                    <div class="d-flex flex-column">
                        <button type="submit" class="log_btn mb-3" data-role="password"> <?php echo e(__('Login')); ?> </button>
                        <?php if(Route::has('password.request')): ?>
                            <a class="" href="<?php echo e(route('password.request')); ?>">
                                <p class="log_label text-center"><?php echo e(__('Forgot Your Password?')); ?></p>
                            </a>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
            <p class="log_header flex-self-center">Content Management System</p>
        </div>

    </div>

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/metro.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    </body>

</html>
<?php /**PATH C:\laragon\www\CMS\resources\views/auth/login.blade.php ENDPATH**/ ?>
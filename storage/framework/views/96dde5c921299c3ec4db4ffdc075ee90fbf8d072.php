<!--dashboard bar-->


<!--title-->
<?php $__env->startSection('title'); ?>
    Settings
<?php $__env->stopSection(); ?>

<!--display-->
<?php $__env->startSection('content'); ?>
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">settings</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">settings</p>
                </div>
            </div>

            <div class="post_con p-4">
                <?php if(session()->has('msg')): ?>
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="d-flex flex-justify-between">
                    <div class="col p-0">
                        <form class="post_form" action="<?php echo e(route('setting.update')); ?>" method="post" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="d-flex flex-justify-between">
                                <div class="col ">
                                    <label class="post_label">Facebook URl</label>
                                    <input type="url" value=" <?php echo e($setting->facebook); ?>" name="facebook" class="post_box <?php $__errorArgs = ['facebook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input">
                                    <?php $__errorArgs = ['facebook'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Twitter URl</label>
                                    <input type="url" value="<?php echo e($setting->twitter); ?> " name="twitter" class="post_box <?php $__errorArgs = ['twitter'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input">
                                    <?php $__errorArgs = ['twitter'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                            </div>
                            <div class="d-flex flex-justify-between">
                                <div class="col ">
                                    <label class="post_label">Instagram URl</label>
                                    <input type="url" value="<?php echo e($setting->Instagram); ?> " name="instagram" class="post_box <?php $__errorArgs = ['instagram'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input">
                                    <?php $__errorArgs = ['instagram'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Google URl</label>
                                    <input type="url" value=" <?php echo e($setting->google); ?> " name="google" class="post_box <?php $__errorArgs = ['google'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" >
                                    <?php $__errorArgs = ['google'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                    <p class="error_msg"><?php echo e($message); ?></p>
                                    <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                </div>
                            </div>
                            <div class="col ">
                                <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                <button type="submit" class="post_btn2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\CMS\resources\views/settings.blade.php ENDPATH**/ ?>
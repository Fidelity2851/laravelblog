<!--dashboard bar-->


<!--title-->
<?php $__env->startSection('title'); ?>
    Post
<?php $__env->stopSection(); ?>

<!--display-->
<?php $__env->startSection('content'); ?>
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">post</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">post</p>
                </div>
            </div>


            <div class="post_con p-4">

                <?php if(session()->has('msg')): ?>
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> <?php echo e(session()->get('msg')); ?></p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                <?php endif; ?>

                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Post</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Post</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                                <div class="col p-0">
                                    <form class="post_form" action="<?php echo e(route('post.store')); ?>" method="post" enctype="multipart/form-data">
                                        <?php echo csrf_field(); ?>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col ">
                                                <label class="post_label">Post Title</label>
                                                <input type="text" value="<?php echo e(old('title')); ?>" name="title" class="post_box <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" required>
                                                <?php $__errorArgs = ['title'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Slug/URl (Optional)</label>
                                                <input type="text" value="<?php echo e(old('slug')); ?>" name="slug" class="post_box <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="input" >
                                                <?php $__errorArgs = ['slug'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col ">
                                                <label class="post_label">Post image</label>
                                                <input type="file" value="<?php echo e(old('image')); ?>" name="image" class="post_box <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="file" required>
                                                <?php $__errorArgs = ['image'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Category</label>
                                                <select id="category" name="category" class="post_sel cate <?php $__errorArgs = ['category'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="select" required>
                                                    <option disabled selected>select a category</option>
                                                    <?php $__currentLoopData = $query1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($data1->id); ?>"><?php echo e($data1->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <?php $__errorArgs = ['category'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Sub-category</label>
                                                <select name="subcategory" class="post_sel sub_cate <?php $__errorArgs = ['subcategory'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" required>
                                                    
                                                </select>
                                                <span class="feedback">Select a Category first</span>
                                                <?php $__errorArgs = ['subcategory'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col-6 ">
                                                <label class="post_label">Summary</label>
                                                <textarea name="summary" class="post_area <?php $__errorArgs = ['summary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="textarea"><?php echo e(old('summary')); ?></textarea>
                                                <?php $__errorArgs = ['summary'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Tags</label>
                                                <input type="text" value="<?php echo e(old('tags')); ?>" name="tags" class="post_box <?php $__errorArgs = ['tags'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="taginput" required>
                                                <?php $__errorArgs = ['tags'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Publision Date</label>
                                                <input type="text" name="pub_date" class="post_box <?php $__errorArgs = ['pub_date'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" data-role="calendarpicker" data-dialog-mode="true" value="<?php echo now(); ?>">
                                                <?php $__errorArgs = ['pub_date'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                                <p class="error_msg"><?php echo e($message); ?></p>
                                                <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                            </div>
                                        </div>
                                        <div class="col ">
                                            <label class="post_label">Description</label>
                                            <textarea id="editor" name="description" class="post_area <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>"><?php echo e(old('description')); ?></textarea>
                                            <?php $__errorArgs = ['description'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                                            <p class="error_msg"><?php echo e($message); ?></p>
                                            <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                                        </div>
                                        <div class="col ">
                                            <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                            <button type="submit" class="post_btn2">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                    <div id="_target_2">
                        <form method="post" action="<?php echo e(route('post.deletemany')); ?>">
                            <?php echo csrf_field(); ?>
                            <div class="d-flex flex-justify-end">
                                <a href="<?php echo e(route('post.list')); ?>" class="mr-3"><button type="button" class="data_g">All Post</button></a>
                                <a href="<?php echo e(route('post.listapprove')); ?>" class="mr-3"><button type="button" class="data_g">Approve Post</button></a>
                                <a href="<?php echo e(route('post.listpending')); ?>" class=""><button type="button" class="data_g">Pending Post</button></a>
                            </div>
                            <table class="table table-border row-border compact striped"
                                   data-check="false"
                                   data-show-search="false"
                                   data-show-rows-steps="false"
                                   data-horizontal-scroll="false"
                                   data-show-pagination="false"
                            >
                                <thead>
                                <tr>
                                    <th class="t_head" > <input id="check_all" name="check_all" type="checkbox" data-role="checkbox"> </th>
                                    <th class="t_head ">ID</th>
                                    <th class="t_head ">Title</th>
                                    <th class="t_head ">Category</th>
                                    <th class="t_head ">Author</th>
                                    <th class="t_head ">Status</th>
                                    <th class="t_head ">Published date</th>
                                    <th class="t_head ">Created date</th>
                                    <th class="t_head">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $query; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $datas): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td class="t_data"><input value="<?php echo e($datas->id); ?>" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                        <td class="t_data"><?php echo e($datas->id); ?></td>
                                        <td class="t_data"><?php echo e($datas->title); ?></td>
                                        <td class="t_data"><?php echo e($datas->category->name); ?></td>
                                        <td class="t_data"><?php echo e($datas->user->username); ?></td>
                                        <td class="t_data">
                                            <?php if($datas->status==1): ?>
                                                <span class="data_g">Approved</span>
                                            <?php elseif($datas->status==0): ?>
                                                <span class="data_w">Pending</span>
                                            <?php endif; ?>
                                        </td>
                                        <td class="t_data"><?php echo e($datas->pub_at); ?></td>
                                        <td class="t_data"><?php echo e($datas->created_at); ?></td>
                                        <td class="t_data">
                                            <?php if($datas->status==0): ?>
                                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('approve', App\post::class)): ?>
                                                    <a href="<?php echo e(route('post.approve', ['post' => $datas->id])); ?>" title="Approve" class="mif-done t_icon"></a>
                                                <?php endif; ?>
                                            <?php elseif($datas->status==1): ?>
                                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('approve', App\post::class)): ?>
                                                    <a href="<?php echo e(route('post.pending', ['post' => $datas->id])); ?>" title="Pending" class="mif-lock t_icon"></a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <a href="<?php echo e(route('post.fetch', ['post' => $datas->id])); ?>" title="Edit" class="mif-open-book t_icon"></a>
                                            <a href="<?php echo e(route('post.delete', ['post' => $datas->id])); ?>" title="Delete" class="mif-bin t_icon con_del"></a>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                            <div class="">
                                <button type="submit" class="del_all_btn mb-3">Delete</button>
                                <?php echo e($query->links()); ?>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\CMS\resources\views/post.blade.php ENDPATH**/ ?>
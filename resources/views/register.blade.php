@extends('layouts/layout')

@section('title')
    Register
@endsection

@section('content')
    <!--body container-->
    <div class="body_con py-0">

        <!--register container-->
        <div class="register_con">
            <div class="container register px-0">
                <div class="register_btn_con">
                    <div class="col-12 col-sm-6 register_btn px-0">
                        <button type="button" class="login_btn btn_active">login</button>
                        <button type="button" class="signup_btn btn_not_active">signup</button>
                    </div>
                </div>
                <div class="register_content_con">
                    <div class="col-11 col-sm-6 col-md-5 col-lg-4 register_content px-0">
                        <div class="col-12 form_con px-0">
                            <form class="col-12 login_form">
                                <p class="form_header">login</p>
                                <label class="form_label">username:</label> <br>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"><img src="images/user.svg" class="form_img"> </span>
                                    <input type="text" class="log_name_box" placeholder="username" required>
                                </div>
                                <label class="form_label">password:</label> <br>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"> <img src="images/pass.svg" class="form_img"> </span>
                                    <input type="password" class="log_password_box" placeholder="password" required>
                                </div>
                                <label class="d-flex "> <input type="checkbox" class="check_box"> <span class="check_text align-self-end">remember me</span> </label>
                                <a href="#" class="text-dark">
                                    <span class="forgot">forgotten password?</span>
                                </a>
                                <div class="login_btn_con">
                                    <button type="submit" class="submit_btn">login</button>
                                </div>
                            </form>
                            <form method="POST" action="{{ route('register') }}" class="col-12 signup_form">
                                @csrf
                                <p class="form_header">signup</p>
                                <label class="form_label">name:</label>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"><img src="images/user.svg" class="form_img"> </span>
                                    <input type="text" name="name" class="log_name_box @error('name') is-invalid @enderror" placeholder="username" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                </div>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <label class="form_label">email:</label>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"><img src="images/email.svg" class="form_img"> </span>
                                    <input type="email" name="email" class="log_name_box @error('email') is-invalid @enderror" placeholder="valid email address" value="{{ old('email') }}" required autocomplete="email">
                                </div>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <label class="form_label">password:</label>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"> <img src="images/pass.svg" class="form_img"> </span>
                                    <input type="password" name="password" class="log_password_box @error('email') is-invalid @enderror" placeholder="password" required autocomplete="new-password">
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <label class="form_label">confirm password:</label>
                                <div class="d-flex justify-content-center mb-3">
                                    <span class="form_icon"> <img src="images/pass.svg" class="form_img"> </span>
                                    <input type="password" name="password_confirmation" class="log_password_box" placeholder="comfirm password" required autocomplete="new-password">
                                </div>

                                <label class="d-flex m-0"> <input type="checkbox" class="check_box" checked required> <span class="check_text align-self-center">i agree to the terms and condition</span> </label>

                                <div class="signup_btn_con">
                                    <button type="submit" class="submit_btn2">signup</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--register container-->

    </div>
    <!--body container ENDS-->
@endsection

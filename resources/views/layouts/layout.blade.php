<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>Tanatech Blog - @yield('title')</title>

    <!--css-->
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style3.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    {{--Livewire style--}}
    <livewire:styles />

    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600&family=Quicksand:wght@300;400;500;600&display=swap" rel="stylesheet">

    <!--fonts icon-->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

</head>
<body>
<div class="housing">
    <!-- Sidebar -->
    <nav id="sidebar" class="position-absolute position-fixed">
        <div id="dismiss">
            <i class="fas fa-arrow-left"></i>
        </div>

        <div class="profile_con d-flex">
            @if(Auth::check())
                @if(Auth::user()->image)
                    <img src="{{ asset('storage/'.Auth::user()->image) }}" class="menu_img mr-3">
                @else
                    <img src="images/user.png" class="menu_img mr-3">
                @endif
                <div class="">
                <p class="menu_name mb-2">{{ Auth::user()->username }}</p>
                <a href="{{ route('logout') }}" class="nav_link text-decoration-none ml-2" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <button type="button" class="log_btn">Logout</button>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
            @endif
        </div>

        <ul class="list-unstyled components">
            <li class="">
                <a href="{{ route('home') }}">Home</a>
            </li>
            @foreach($categorys as $category)
                <li>
                    <a href="#{{ $category->name }}" data-toggle="collapse" aria-expanded="false">{{ $category->name }}</a>
                    <ul class="collapse list-unstyled" id="{{ $category->name }}">
                        @foreach($category->subcategory as $subcategory)
                            <li>
                                <a href="{{ route('posts.subcategory', ['id'=>$subcategory->id]) }}">{{ $subcategory->name }} {{ $category->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
            <li>
                <a href="{{ route('about') }}">About</a>
            </li>
            <li>
                <a href="{{ route('contact') }}">Contact</a>
            </li>
            @if(Auth::check())
                @can('viewAny', App\User::class)
                    <li>
                        <a href="{{ route('admin') }}" target="_blank">Admin Panel</a>
                    </li>
                @endcan
            @else
                <li>
                    <a href="{{ route('admin') }}">Login</a>
                </li>
            @endif
        </ul>

    </nav>
    <div class="overlay"></div>

    <!--scroll to top-->
    <button type="button" class="scroll_top"> <img src="{{ asset('images/scroll_top.svg') }}" class="scroll_img"> </button>
    <!--scroll to top ENDS-->

    <!--search container-->
    <div class="search_con">
        <div class="container search d-flex justify-content-center px-0">
            <livewire:search-posts />
        </div>
    </div>
    <!--search container ENDS-->

    <!--header SMALL screen-->
    <div class="row header_con_mob d-lg-none sticky-top mx-0">
        <div class="container header_mob">
            <div class="menubar_con_mob p-0">
                <button id="sidebarCollapse" type="button" class="menu_btn p-1"><i class="fa fa-bars"></i> </button>
            </div>
            <div class="logo_con_mob">
                <a href="{{ route('home') }}" class="text-decoration-none"> <img src="{{ asset('images/logo.fw.png') }}" class="logo"> </a>
            </div>
            <div class="search_con_mob">
                <button type="button" class="notify magic"> <i class="fa fa-search"></i> s</button>
            </div>
        </div>

    </div>
    <!--header SMALL screen ENDS-->

    <!--header BIG screen-->
    <div class="header_con d-none d-lg-block sticky-top">
        <div class="container header d-flex justify-content-between px-0">
            <div class="logo_con align-self-center">
                <a href="{{ route('home') }}" class="text-decoration-none"> <img src="{{ asset('images/logo.fw.png') }}" class="logo"> </a>
            </div>
            <div class="col top_link_con d-flex justify-content-end px-0">
                <a href="{{ route('home') }}" class="nav_link text-decoration-none">
                    <p class="link">home</p>
                </a>
                @foreach($categorys as $category)
                    <div class="drop_con">
                        <a href="{{ route('posts.category', ['id'=>$category->id]) }}" class="nav_link text-decoration-none">
                            <p class="link">{{ $category->name }}</p>
                        </a>
                        <div class="drop_content">
                            @foreach($category->subcategory as $subcategory)
                                <a href="{{ route('posts.subcategory', ['id'=>$subcategory->id]) }}" class="text-decoration-none">
                                    <p class="link_drop">{{ $subcategory->name }} {{ $category->name }}</p>
                                </a>
                            @endforeach
                        </div>
                    </div>

                    {{--<div class="drop_con">
                        <a href="{{ route('videos') }}" class="nav_link text-decoration-none">
                            <p class="link">{{ $category->name }}</p>
                        </a>
                        <div class="drop_content">
                            <div class="indic"></div>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">musics videos</p>
                            </a>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">entertainment videos</p>
                            </a>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">funny videos</p>
                            </a>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">sports videos</p>
                            </a>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">Motivational videos</p>
                            </a>
                            <a href="{{ route('videos') }}" class="text-decoration-none">
                                <p class="link_drop">others</p>
                            </a>
                        </div>
                    </div>

                    <div class="drop_con">
                        <a href="{{ route('news') }}" class="nav_link text-decoration-none">
                            <p class="link">{{ $category->name }}</p>
                        </a>
                        <div class="drop_content">
                            <div class="indic"></div>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">polities news</p>
                            </a>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">entertainment news</p>
                            </a>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">sports news</p>
                            </a>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">tech news</p>
                            </a>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">gossip news</p>
                            </a>
                            <a href="{{ route('news') }}" class="text-decoration-none">
                                <p class="link_drop">others</p>
                            </a>
                        </div>
                    </div>

                    <div class="drop_con">
                        <a href="{{ route('status') }}" class="nav_link text-decoration-none">
                            <p class="link">{{ $category->name }}</p>
                        </a>
                        <div class="drop_content">
                            <div class="indic"></div>
                            <a href="{{ route('status') }}" class="text-decoration-none">
                                <p class="link_drop">funny status</p>
                            </a>
                            <a href="{{ route('status') }}" class="text-decoration-none">
                                <p class="link_drop">motivational status</p>
                            </a>
                            <a href="{{ route('status') }}" class="text-decoration-none">
                                <p class="link_drop">educational status</p>
                            </a>
                            <a href="{{ route('status') }}" class="text-decoration-none">
                                <p class="link_drop">romatic status</p>
                            </a>
                            <a href="{{ route('status') }}" class="text-decoration-none">
                                <p class="link_drop">others</p>
                            </a>
                        </div>
                    </div>

                    <div class="drop_con">
                        <a href="{{ route('stories') }}" class="nav_link text-decoration-none">
                            <p class="link">{{ $category->name }}</p>
                        </a>
                        <div class="drop_content">
                            <div class="indic"></div>
                            <a href="{{ route('stories') }}" class="text-decoration-none">
                                <p class="link_drop">educational stories</p>
                            </a>
                            <a href="{{ route('stories') }}" class="text-decoration-none">
                                <p class="link_drop">motivational stories</p>
                            </a>
                            <a href="{{ route('stories') }}" class="text-decoration-none">
                                <p class="link_drop">romantic stories</p>
                            </a>
                            <a href="{{ route('stories') }}" class="text-decoration-none">
                                <p class="link_drop">folks stories</p>
                            </a>
                            <a href="{{ route('stories') }}" class="text-decoration-none">
                                <p class="link_drop">others</p>
                            </a>
                        </div>
                    </div>--}}
                @endforeach

                <a href="{{ route('about') }}" class="nav_link text-decoration-none">
                    <p class="link">about</p>
                </a>

                <a href="{{ route('contact') }}" class="nav_link text-decoration-none">
                    <p class="link">contact</p>
                </a>

                <p class="link align-self-center magic"> <i class="fa fa-search"></i> </p>

                @if(Auth::check())
                    <div class="drop_con">
                        <a href="#" class="nav_link text-decoration-none d-flex">
                            @if(Auth::user()->image)
                                <img src="{{ asset('../storage/'.Auth::user()->image) }}" class="menu_img align-self-center">
                            @else
                                <img src="{{ asset('images/user.png') }}" class="menu_img align-self-center">
                            @endif
                            <p class="link"> {{ Auth::user()->username }} </p>
                        </a>
                        <div class="drop_content">
                            @can('viewAny', App\User::class)
                                <a href="{{ route('admin') }}" class="text-decoration-none" target="_blank">
                                    <p class="link_drop">Admin Panel</p>
                                </a>
                            @endcan
                            <a href="{{ route('logout') }}" class="text-decoration-none" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <p class="link_drop">Logout</p>
                            </a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                @else
                    <a href="{{ route('admin') }}" class="nav_link text-decoration-none ml-2">
                        <button type="button" class="nav_btn">Login</button>
                    </a>
                @endif


            </div>
        </div>
    </div>
    <!--header ENDS-->

    @yield('content')

    <!--footer container-->
    <div class="row footer_con mx-0">
        <div class="container footer d-flex justify-content-center  p-0">
            <div class="col-12 col-lg-7 footer_content_con1 px-0">
                <div class="col footer_link_con px-0">
                    <ul class="footer_link list-unstyled">
                        <p class="footer_link_header">abouts</p>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">parterns</li>
                        </a>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">who we are</li>
                        </a>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">how we work</li>
                        </a>
                    </ul>
                </div>
                <div class="col footer_link_con px-0">
                    <ul class="footer_link list-unstyled">
                        <p class="footer_link_header">trending</p>
                    </ul>
                </div>
                <div class="col footer_link_con px-0">
                    <ul class="footer_link list-unstyled">
                        <p class="footer_link_header">services</p>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">cookies</li>
                        </a>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">privacy</li>
                        </a>
                        <a href="#" class="text-decoration-none">
                            <li class="link_con">terms & condition</li>
                        </a>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container copy_right_con text-center d-block d-sm-flex justify-content-sm-between">
            <p class="copy_right">copyright @2019. all right reserved</p>
            <p class="powered_by">powered by:
                <a href="#" class="text-decoration-none">
                    <span class="power"> tanatech Labs LTD</span>
                </a>
            </p>
        </div>
    </div>
    <!--footer container ENDS-->
</div>

{{--Livewire scripts--}}
<livewire:scripts />

{{--Javascript files--}}
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<!--Menu bar plugin-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        $("#sidebar").mCustomScrollbar({
            theme: "minimal"
        });

        $('#dismiss, .overlay').on('click', function () {
            $('#sidebar').removeClass('active');
            $('.overlay').removeClass('active');
        });

        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').addClass('active');
            $('.overlay').addClass('active');
            $('.collapse.in').toggleClass('in');
            $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        });
    });
</script>

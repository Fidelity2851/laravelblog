<!--banner-->
<div class="row banner_con mt-md-3 mx-0">
    <div class="container banner d-lg-flex justify-content-between px-0">
        <div class="col banner1 d-flex justify-content-center px-0">
            <div id="demo" class="col carousel slide px-0 mr-lg-3 mb-0 mb-md-3 mb-lg-0" data-ride="carousel">
                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($banners as $banner)
                        @if($loop->first)
                        <div class="carousel-item active ban">
                            <a href="{{ route('read', ['id'=>$banner->id]) }}" class="text-decoration-none">
                                <span class="video_icon"> <i class="fa fa-play"></i> </span>
                                <img src="{{ asset('storage/'.$banner->image) }}" class="banner_img_lg">
                                <div class="banner_text_con">
                                    <p class="banner_text_lg">{{ $banner->title }}</p>
                                </div>
                            </a>
                        </div>
                        @else
                        <div class="carousel-item ban">
                            <a href="{{ route('read', ['id'=>$banner->id]) }}" class="text-decoration-none">
                                <span class="video_icon"> <i class="fa fa-play"></i> </span>
                                <img src="{{ asset('storage/'.$banner->image) }}" class="banner_img_lg">
                                <div class="banner_text_con">
                                    <p class="banner_text_lg">{{ $banner->title }}</p>
                                </div>
                            </a>
                        </div>
                        @endif
                    @endforeach
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="banner_arrow"> <i class="fa fa-angle-left"></i> </span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="banner_arrow"> <i class="fa fa-angle-right"></i> </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 banner2 row justify-content-between mx-0 px-0">
            @foreach($small_banners as $small_banner)
                <div class="col-6 col-md-3 col-lg-6 banner_2 px-0">
                    <a href="{{ route('read', ['id'=>$small_banner->id]) }}" class="text-decoration-none">
                        <div class="video_post_con">
                            <span class="video_icon"> <i class="fa fa-music"></i> </span>
                            <img src="{{ asset('storage/'.$small_banner->image) }}" class="banner_img_md">
                            <div class="banner_text_con">
                                <p class="post_text_md">{{ $small_banner->title }}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!--banner ENDS-->

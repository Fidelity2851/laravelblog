<div>
    <form method="post" action="{{ route('post.deletemany') }}">
        @csrf
        <div class="d-flex flex-justify-between">
            <div class="flex-self-center">
                <div class="d-flex justify-content-between">
                    <input type="search" wire:model="search" class="search_box mr-2 mr-md-4" placeholder="Search your posts....">
                    <button type="button" wire:click="doreset" class="search_btn1">reset</button>
                </div>
            </div>
            <div class="flex-self-center">
                <button wire:click="all_post" type="button" class="data_g">All Post</button>
                <button wire:click="approved_post" type="button" class="data_g">Approve Post</button>
                <button wire:click="pending_post" type="button" class="data_g">Pending Post</button>
            </div>
        </div>
        <div wire:loading wire:target="search, all_post, approved_post, pending_post" class="col-12 ">
            <div class="d-flex flex-justify-center pt-3">
                <div class="">
                    <span class="mif-spinner ani-spin mif-3x fg-success"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-primary"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-warning"></span>
                </div>
            </div>
        </div>
        <table class="table table-border row-border compact striped"
               data-check="false"
               data-show-search="false"
               data-show-rows-steps="false"
               data-horizontal-scroll="false"
               data-show-pagination="false"
        >
            <thead>
            <tr>
                <th class="t_head" > <input id="check_all" name="check_all" type="checkbox" data-role="checkbox"> </th>
                <th class="t_head ">#</th>
                <th class="t_head ">Title</th>
                <th class="t_head ">Category</th>
                <th class="t_head ">Author</th>
                <th class="t_head ">Status</th>
                <th class="t_head ">Views</th>
                <th class="t_head ">Published date</th>
                <th class="t_head ">Created date</th>
                <th class="t_head">Action</th>
            </tr>
            </thead>
            <tbody>

            @if(count($query) > 0)
                @foreach($query as $datas)
                    <tr>
                        <td class="t_data"><input value="{{$datas->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                        <td class="t_data">{{$loop->iteration}}</td>
                        <td class="t_data">{{$datas->title}}</td>
                        <td class="t_data">{{$datas->category->name}}</td>
                        <td class="t_data">{{$datas->user->username}}</td>
                        <td class="t_data">
                            @if($datas->status==1)
                                <span class="data_g">Approved</span>
                            @elseif($datas->status==0)
                                <span class="data_w">Pending</span>
                            @endif
                        </td>
                        <td class="t_data">{{$datas->views}}</td>
                        <td class="t_data">{{$datas->pub_at}}</td>
                        <td class="t_data">{{$datas->created_at}}</td>
                        <td class="t_data">
                            @if($datas->status==0)
                                @can('approve', App\post::class)
                                    <span wire:click="approve({{ $datas->id }})" title="Approve" class="mif-done t_icon"></span>
                                @endcan
                            @elseif($datas->status==1)
                                @can('approve', App\post::class)
                                    <span wire:click="pending({{ $datas->id }})" title="Pending" class="mif-lock t_icon"></span>
                                @endcan
                            @endif
                            <a href="{{ route('post.fetch', ['post' => $datas->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                            <a href="{{ route('post.delete', ['post' => $datas->id]) }}" title="Delete" class="mif-bin t_icon con_del"></a>
                        </td>
                    </tr>
                @endforeach
            @else
                <p class="search_head1 text-center">No Result Found...</p>
            @endif
            </tbody>
        </table>
        <div class="">
            <button type="submit" class="del_all_btn mb-3">Delete</button>
            {{ $query->links() }}
        </div>
    </form>
</div>

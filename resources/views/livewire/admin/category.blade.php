<div>

    <form method="post" action="{{ route('category.deletemany') }}">
        @csrf
        <div class="d-flex flex-justify-start">
            <div class="flex-self-center">
                <div class="d-flex justify-content-between">
                    <input type="search" wire:model="search" class="search_box mr-2 mr-md-4" placeholder="Search your categories....">
                    <button type="button" wire:click="doreset" class="search_btn1">reset</button>
                </div>
            </div>
        </div>
        <div wire:loading wire:target="search" class="col-12 ">
            <div class="d-flex flex-justify-center pt-3">
                <div class="">
                    <span class="mif-spinner ani-spin mif-3x fg-success"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-primary"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-warning"></span>
                </div>
            </div>
        </div>
        <table class="table table-border row-border compact striped"
               data-check="false"
               data-show-search="false"
               data-show-rows-steps="false"
               data-horizontal-scroll="false"
               data-show-pagination="false"
        >
            <thead>
            <tr>
                <th class="t_head" > <input name="check_all" type="checkbox" data-role="checkbox"> </th>
                <th class="t_head sortable-column" >ID</th>
                <th class="t_head sortable-column" >Category</th>
                <th class="t_head sortable-column" >Description</th>
                <th class="t_head sortable-column" >Created date</th>
                <th class="t_head">Action</th>
            </tr>
            </thead>
            @if(count($query) > 0)
                <tbody>
                @foreach($query as $data)
                    <tr>
                        <td class="t_data"><input value="{{$data->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                        <td class="t_data">{{$loop->iteration}}</td>
                        <td class="t_data">{{$data->name}}</td>
                        <td class="t_data">{{$data->description}}</td>
                        <td class="t_data">{{$data->created_at}}</td>
                        <td class="t_data">
                            <a href="{{ route('category.fetch', ['category' => $data->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                            <a title="Delete" href="{{ route('category.delete', ['category' => $data->id]) }}" class="mif-bin t_icon con_del"> </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @else
                <p class="search_head1 text-center">No Result Found...</p>
            @endif
        </table>
        <div class="">
            <button type="submit" class="del_all_btn mb-3">Delete</button>
            {{ $query->links() }}
        </div>
    </form>

</div>

<div>
    <form method="post" action="{{ route('banner.deletemany') }}">
        @csrf
        <div class="d-flex flex-justify-between">
            <div class="flex-self-center">
                <div class="d-flex justify-content-between">
                    <input type="search" wire:model="search" class="search_box mr-2 mr-md-4" placeholder="Search your posts....">
                    <button type="button" wire:click="doreset" class="search_btn1">reset</button>
                </div>
            </div>
        </div>
        <div wire:loading wire:target="search" class="col-12 ">
            <div class="d-flex flex-justify-center pt-3">
                <div class="">
                    <span class="mif-spinner ani-spin mif-3x fg-success"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-primary"></span>
                    <span class="mif-spinner ani-spin mif-3x fg-warning"></span>
                </div>
            </div>
        </div>
        <table class="table table-border row-border compact striped"
               data-check="false"
               data-show-search="false"
               data-show-rows-steps="false"
               data-horizontal-scroll="false"
               data-show-pagination="false"
        >
            <thead>
            <tr>
                <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                <th class="t_head" data-sortable="true">ID</th>
                <th class="t_head" data-sortable="true">Title</th>
                <th class="t_head" data-sortable="true">Image</th>
                <th class="t_head" data-sortable="true">Description</th>
                <th class="t_head" data-sortable="true">Created date</th>
                <th class="t_head">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td class="t_data"><input value="{{$banner->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$banner->title}}</td>
                    <td><img class="post_img" src="{{asset('storage/'.$banner->image)}}"> </td>
                    <td>{{$banner->description}}</td>
                    <td>{{$banner->created_at}}</td>
                    <td>
                        <a href="{{ route('banner.fetch', ['banner' => $banner->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                        <a href="{{ route('banner.delete', ['banner' => $banner->id]) }}" title="Delete" class="mif-bin t_icon con_del"></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="">
            <button type="submit" class="del_all_btn mb-3">Delete</button>
            {{ $banners->links() }}
        </div>
    </form>
</div>

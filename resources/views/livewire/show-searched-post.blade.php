<div class="">
    <div class="col form_content">
        @if(!empty($posts))
            @foreach($posts as $post)
                <div class="d-flex mb-3">
                    <img src="{{ asset('storage/'.$post->image) }}" class="post_img_sm mr-3">
                    <div class="col px-0">
                        <a href="{{ route('read', ['id'=>$post->id]) }}" class="text-decoration-none"> <p class="post_text">{{$post->title}}</p> </a>
                        <div class="d-flex justify-content-between">
                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                            <a href="{{ route('posts.subcategory', ['id'=>$post->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$post->subcategory->name}} {{$post->category->name}}</span></a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p class="post_text text-center">No Result Found...</p>
        @endif
    </div>
    <div class="col pagi_b pt-3">
        {{ $posts->links() }}
    </div>
</div>

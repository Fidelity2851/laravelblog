<div class="col-12 col-sm-10 col-lg-6 search_form_con align-self-center px-0">
    <form wire:submit.prevent="searching" method="post" class="col search_form shadow-sm sticky-top p-4">
        <div class="d-flex justify-content-between">
            <input type="search" wire:model="search" name="search" class="search_box mr-2 mr-md-4" placeholder="Search the site...." required>
            <button type="submit" class="search_btn1">search</button>
        </div>
    </form>
    <div class="col form_content">
        {{--<div wire:loading.remove wire:target="search" class="col ">

        </div>--}}
        <div wire:loading wire:target="searching" class="col ">
            <div class="d-flex justify-content-center pt-3">
                <div class="spinner-grow text-success" role="Status">
                    <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-primary" role="Status">
                    <span class="sr-only">Loading...</span>
                </div>
                <div class="spinner-grow text-warning" role="Status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
        @if(!empty($posts))
            @foreach($posts as $post)
                <div class="d-flex mb-3">
                    <img src="{{ asset('storage/'.$post->image) }}" class="post_img_sm mr-3">
                    <div class="col px-0">
                        <a href="{{ route('read', ['id'=>$post->id]) }}" class="text-decoration-none"> <p class="post_text">{{$post->title}}</p> </a>
                        <div class="d-flex justify-content-between">
                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                            <a href="{{ route('posts.subcategory', ['id'=>$post->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$post->subcategory->name}} {{$post->category->name}}</span></a>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <p class="search_head1 text-center">No Result Found...</p>
        @endif
        @error('search') <p class="search_head1 text-center">{{$message}}</p> @enderror
    </div>
    <div class="col ">
        @if(!empty($posts))
            {{$posts->links()}}
        @endif
    </div>
</div>

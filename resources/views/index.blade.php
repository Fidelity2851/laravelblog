@extends('layouts/layout')

@section('title')
    Home
@endsection

@section('content')

    @include('banner')

    <!--body container-->
    <div class="body_con mt-3">
        <div class="container body_content_con d-lg-flex justify-content-between px-0">
            <!--first content container-->
            <div class="col-12 col-lg-8 body1 border-right-0 pl-0 pr-0 pr-lg-3">
                <!--latest post container-->
                <div class="mb-5">
                    <div class="col latest_header_con">
                        <p class="latest_header align-self-center">latest news</p>
                    </div>
                    <div class="">
                        <div class="row latest_row justify-content-around justify-content-md-start mx-0">
                            @forelse($news as $new)
                                <div class="col-12 col-sm-10 col-md-4 px-md-2 mb-4">
                                    <div class="col latest_post px-0">
                                        <div class="mb-2">
                                            <span class="view" title="Views">{{ $new->views }}</span>
                                            <img src="../storage/{{ $new->image }}" class="latest_post_img">
                                        </div>
                                        <div class="">
                                            <a href="{{ route('read', ['id'=>$new->id]) }}" class="text-decoration-none"> <p class="latest_post_text">{{ $new->title }}</p> </a>
                                            <div class="d-flex justify-content-between">
                                                <span class="time align-self-center"> <i class="fa fa-clock"></i> {{ date('i') - date('i', strtotime($new->created_at)) }} mins ago </span>
                                                <a href="{{ route('posts.subcategory', ['id'=>$new->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{ $new->subcategory->name }} {{ $new->category->name }}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="latest_post_text ">No Record To Show</p>
                            @endforelse
                        </div>
                        @foreach($news as $new)
                            @if($loop->first)
                                <div class="d-flex justify-content-center">
                                    <a href="{{route('posts.category', ['id'=>$new->category->id])}}" class="text-decoration-none mx-auto"> <span class="more">view all</span> </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <!--latest post container ENDS-->

                <div class="d-none d-md-block advert_con mb-5">
                    <div class="advert2">
                        <img src="images/advert.gif" class="advert2">
                    </div>
                </div>
                <div class="d-md-none advert_con mb-5">
                    <div class="advert3">
                        <img src="images/advert.gif" class="advert3">
                    </div>
                </div>

                <!--music post container-->
                <div class="mb-5">
                    <div class="latest_header_con ">
                        <p class="latest_header align-self-center">hot music</p>
                    </div>
                    <div class="col post_row d-md-flex justify-content-between px-md-0 mb-3">
                        @foreach($musics as $music)
                            @if($loop->first)
                                <div class="col post1_con pl-0 pr-0 pr-md-3 mb-4">
                                    <div class="mb-2">
                                        <span class="view">{{$music->views}}</span>
                                        <img src="{{ asset('storage/'.$music->image) }}" class="post_img_lg">
                                    </div>
                                    <div class="">
                                        <a href="{{ route('read', ['id'=>$music->id]) }}" class="text-decoration-none"> <p class="post_text_lg">{{$music->title}}</p> </a>
                                        <p class="post_desc">{{$music->summary}}</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="{{ route('posts.subcategory', ['id'=>$music->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i>{{$music->subcategory->name}} {{$music->category->name}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="col post2_con pr-0 pl-0 pl-md-3">
                            <div class="">
                                @foreach($musics as $music)
                                    @continue($loop->first)
                                    <div class="d-flex mb-3">
                                        <img src="{{ asset('storage/'.$music->image) }}" class="post_img_sm mr-3">
                                        <div class="col px-0">
                                            <a href="{{ route('read', ['id'=>$music->id]) }}" class="text-decoration-none"> <p class="post_text">{{$music->title}}</p> </a>
                                            <div class="d-flex justify-content-between">
                                                <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                                <a href="{{ route('posts.subcategory', ['id'=>$music->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i>{{$music->subcategory->name}} {{$music->category->name}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @foreach($musics as $music)
                        @if($loop->first)
                            <div class="d-flex justify-content-center">
                                <a href="{{route('posts.category', ['id'=>$music->category->id])}}" class="text-decoration-none mx-auto"> <span class="more">view all</span> </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!--music post container ENDS-->

                <!--videos post container-->
                <div class="mb-5">
                    <div class="latest_header_con">
                        <p class="latest_header align-self-center">videos</p>
                    </div>
                    <div class="col post_row row px-0 mx-0">
                        @forelse($videos as $video)
                            <div class="col-12 col-md-6 px-md-2">
                                <div class="col video_post_con px-0">
                                    <span class="view">{{$video->views}}</span> <span class="video_icon"> <i class="fa fa-play"></i> </span>
                                    <img src="{{ asset('storage/'.$video->image) }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <a href="{{ route('read', ['id'=>$video->id]) }}"><p class="post_text_md">{{$video->title}}</p></a>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="{{ route('posts.subcategory', ['id'=>$video->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-play"></i> {{$video->subcategory->name}} {{$video->category->name}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p class="latest_post_text text-center">No Record To Show</p>
                        @endforelse
                    </div>
                    @foreach($videos as $video)
                        @if($loop->first)
                            <div class="d-flex justify-content-center">
                                <a href="{{route('posts.category', ['id'=>$video->category->id])}}" class="text-decoration-none mx-auto"> <span class="more">view all</span> </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!--videos post container ENDS-->

                <div class="d-md-none advert_con mb-5">
                    <div class="advert3">
                        <img src="images/advert.gif" class="advert3">
                    </div>
                </div>

                <!--stories post container-->
                <div class="mb-5">
                    <div class="latest_header_con">
                        <p class="latest_header align-self-center">stories</p>
                    </div>
                    <div class="col post_row d-md-flex justify-content-between px-md-0">
                        @foreach($stories as $storie)
                            @if($loop->first)
                                <div class="col post1_con pl-0 pr-0 pr-md-3 mb-4">
                                    <div class="mb-2">
                                        <span class="view">{{$storie->views}}</span>
                                        <img src="{{asset('storage/'.$storie->image)}}" class="post_img_lg">
                                    </div>
                                    <div class="">
                                        <a href="{{ route('read',['id'=>$storie->id]) }}" class="text-decoration-none"> <p class="post_text_lg">{{$storie->title}}</p> </a>
                                        <p class="post_desc">{{$storie->summary}}</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="{{ route('posts.subcategory',['id'=>$storie->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$storie->subcategory->name}} {{$storie->category->name}}</span></a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        <div class="col post2_con pr-0 pl-0 pl-md-3">
                            <div class="">
                                @foreach($stories as $storie)
                                    @continue($loop->first)
                                    <div class="d-flex mb-3">
                                        <img src="{{ asset('storage/'.$storie->image) }}" class="post_img_sm mr-3">
                                        <div class="col px-0">
                                            <a href="{{ route('read',['id'=>$storie->id]) }}" class="text-decoration-none"> <p class="post_text">{{$storie->title}}</p> </a>
                                            <div class="d-flex justify-content-between">
                                                <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                                <a href="{{ route('posts.subcategory',['id'=>$storie->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$storie->subcategory->name}} {{$storie->category->name}}</span></a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    @foreach($stories as $storie)
                        @if($loop->first)
                            <div class="d-flex justify-content-center">
                                <a href="{{route('posts.category', ['id'=>$storie->category->id])}}" class="text-decoration-none mx-auto"> <span class="more">view all</span> </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!--stories post container ENDS-->

                <div class="d-none d-md-block advert_con mb-5">
                    <div class="advert2">
                        <img src="images/advert.gif" class="advert2">
                    </div>
                </div>
                <div class="d-md-none advert_con mb-5">
                    <div class="advert3">
                        <img src="images/advert.gif" class="advert3">
                    </div>
                </div>

                <!--status post container-->
                <div class="mb-5">
                    <div class="latest_header_con">
                        <p class="latest_header align-self-center">status</p>
                    </div>
                    <div class="row post_row mx-0">
                        @forelse($status as $statu)
                            <div class="col-12 col-md-4 video_post_con px-0">
                            <span class="view">{{$statu->views}}</span> <span class="video_icon"> <i class="fa fa-picture-o"></i> </span>
                            <img src="{{ asset('storage/'.$statu->image) }}" class="post_img_md">
                            <div class="video_text_con">
                                <a href="{{ route('read', ['id'=>$statu->id]) }}" class=""><p class="post_text_md">{{$statu->title}}</p></a>
                                <div class="d-flex justify-content-between">
                                    <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                    <a href="{{ route('posts.subcategory',['id'=>$statu->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-picture-o"></i> {{$statu->subcategory->name}} {{$statu->category->name}}</span></a>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p class="latest_post_text text-center">No Record To Show</p>
                        @endforelse
                    </div>
                    @foreach($status as $statu)
                        @if($loop->first)
                            <div class="d-flex justify-content-center">
                                <a href="{{route('posts.category', ['id'=>$statu->category->id])}}" class="text-decoration-none mx-auto"> <span class="more">view all</span> </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                <!--status post container ENDS-->

            </div>
            <!--first content container ENDS-->

            <!--second content container -->
            <div class="col-12 col-lg d-md-flex d-lg-block body2 pr-md-0 pl-md-0 pl-lg-3">
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="aside_post_header_con border-bottom">
                            <p class="aside_post_header">Trending Posts</p>
                        </div>
                        @forelse($favs as $fav)
                            <div class="d-flex mb-3">
                            <img src="{{ asset('storage/'.$fav->image) }}" class="post_img_sm mr-3">
                            <div class="col px-0">
                                <a href="{{ route('read', ['id'=>$fav->id]) }}" class="text-decoration-none"> <p class="post_text">{{$fav->title }}</p> </a>
                                <div class="d-flex justify-content-between">
                                    <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                    <a href="{{ route('posts.subcategory', ['id'=>$fav->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$fav->subcategory->name}} {{$fav->category->name}}</span></a>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p class="post_text text-center mb-3">No Record To Show</p>
                        @endforelse
                    </div>
                </div>
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                    <div class="col pr-0 pl-0 pl-md-3 pl-lg-0">
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">song of the week</p>
                            </div>
                            <a href="reading.blade.php" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">10</span> <span class="video_icon"> <i class="fa fa-music"></i> </span>
                                    <img src="images/music_img1.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="#" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-play"></i> video</span></a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">artiste of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">2.9k</span>
                                    <img src="images/artist_img.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">blog queen of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <img src="images/artist_img.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                </div>
            </div>
            <!--second content container ENDS-->
        </div>
    </div>
    <!--body container ENDS-->
@endsection



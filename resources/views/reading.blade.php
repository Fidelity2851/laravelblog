@extends('layouts/layout')

@section('title')
    {{ $posts->title }}
@endsection

@section('content')
    <!--body container-->
    <div class="row body_con mt-3 mx-0">
        <div class="container body_content_con d-lg-flex justify-content-between px-0">
            <!--first content container-->
            <div class="col-12 col-lg-8 body1 border-right-0 pl-0 pr-0 pr-lg-3">
                <!--reading post container-->
                <div class="col mb-5 px-md-0">
                    <div class="latest_header_con border-bottom d-flex">
                        <a href="{{ route('posts.category', $posts->category->id) }}" class="text-decoration-none"> <p class="post_head align-self-center">{{ $posts->category->name }}</p> </a>
                        <span class="time align-self-center mx-3"> <i class="fa fa-clock"></i> {{ date('i') - date('i', strtotime($posts->created_at)) }} mins ago </span>
                        <span class="time align-self-center mr-3"> <i class="fa fa-eye"></i> {{ $posts->views }} views</span>
                        <span class="time align-self-center"> <i class="fa fa-eye"></i> {{ $posts->comment->count() }} Comment</span>
                    </div>
                    <div class="d-flex mb-2">
                        @if($posts->user->image)
                            <img src="{{ asset('../storage/'.$posts->user->image) }}" class="menu_img align-self-center mr-3">
                        @else
                            <img src="{{ asset('images/user.png') }}" class="menu_img align-self-center mr-3">
                        @endif
                        <p class="author_name align-self-center">{{ $posts->user->username }}</p>
                    </div>
                    <p class="post_header">{{ $posts->title }}</p>
                    <div class="d-sm-flex mb-3">
                        <p class="post_share align-self-center">share on :</p>
                        <div class="d-flex">
                            <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/facebook.svg') }}" class="socail_icon"> </a> </span>
                            <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/twitter.svg') }}" class="socail_icon"> </a> </span>
                            <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/instagram_img.svg') }}" class="socail_icon"> </a> </span>
                            <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/whatsapp.svg') }}" class="socail_icon"> </a> </span>
                            <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/youtube.svg') }}" class="socail_icon"> </a> </span>
                        </div>
                    </div>
                    <div class="d-none d-md-block advert_con mb-5">
                        <div class="advert2">
                            <img src="{{ asset('images/advert.gif') }}" class="advert2">
                        </div>
                    </div>
                    <div class="d-md-none advert_con mb-5">
                        <div class="advert3">
                            <img src="{{ asset('images/advert.gif') }}" class="advert3">
                        </div>
                    </div>
                    <div class="mb-5">
                        <p class="single_post_text">{{ strip_tags(htmlspecialchars_decode($posts->summary)) }}</p>
                        <img src="{{ asset('storage/'.$posts->image) }}" class="single_post_img">
                        <p class="single_post_text">{{ strip_tags(htmlspecialchars_decode($posts->description)) }}</p>
                        <div class="">
                            @if($posts->file)
                                <audio src="{{asset('storage/'.$posts->file)}}" class="music_player" controls></audio>
                            @endif
                            <div class="">
                                <div class="col mb-4 px-md-0">
                                    <button type="button" class="download_btn align-self-center mb-3">Download Mp3</button>
                                    <div class="align-self-center ">
                                        <button type="button" class="like"> <i class="far fa-thumbs-up"></i> </button>
                                        <button type="button" class="hate"> <i class="far fa-thumbs-down"></i> </button>
                                    </div>
                                </div>
                                <div class="col tag_con mb-3 px-0">
                                    @foreach($posts->tag as $tag)
                                        <a href="{{ route('posts.tag', ['id'=>$tag->post->id]) }}" class=""> <span class="tag_link">{{ $tag->name }}</span> </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="comment_form_con mb-5">
                        <p class="comment_header border-bottom">leave a comment</p>
                        <div class="">
                            @if(Auth::check())
                                <form action="{{ route('create.comment', ['id'=>$posts->id]) }}" method="post" class="col col-md-6 comment_form mb-5 px-md-0">
                                @csrf
                                    <div class="mb-3">
                                        <textarea class="comment_message" name="comment" placeholder="Drop your comments..." required></textarea>
                                    </div>
                                    <button type="submit" name="commment_btn" class="comment_btn">Send</button>
                                </form>
                            @endif
                            <div class="col d-flex justify-content-center px-0">
                                <div class="card text-left w-100">
                                    <div class="card-header">
                                        COMMENTS
                                    </div>
                                    <div class="overflow-auto" style="height: 300px;">
                                        @forelse($posts->comment as $comment)
                                            <div class="card-body shadow-sm">
                                                <p class="latest_post_text mb-2">{{ $comment->name }}</p>
                                                <div class="d-flex justify-content-between">
                                                    <p class="type mb-0"><i class="fa fa-user"></i> {{ $comment->user->username }}</p>
                                                    <p class="type mb-0"><i class="fa fa-clock"></i> {{ date('d/M/Y', strtotime($comment->created_at)) }}</p>
                                                </div>
                                            </div>
                                        @empty
                                            <p class="latest_post_text text-center py-3">NO COMMENT TO SHOW</p>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-none d-md-block advert_con mb-5">
                        <div class="advert2">
                            <img src="{{ asset('images/advert.gif') }}" class="advert2">
                        </div>
                    </div>
                    <div class="d-md-none advert_con mb-5">
                        <div class="advert3">
                            <img src="{{ asset('images/advert.gif') }}" class="advert3">
                        </div>
                    </div>
                </div>
            </div>
            <!--first content container ENDS-->

            <!--second content container -->
            <div class="col-12 col-lg d-md-flex d-lg-block body2 pr-md-0 pl-md-0 pl-lg-3">
                <div class="col px-0">
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="mb-3">
                            <p class="banner_header">connect with us</p>
                            <div class="d-flex justify-content-start">
                                <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/facebook.svg') }}" class="socail_icon"> </a> </span>
                                <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/twitter.svg') }}" class="socail_icon"> </a> </span>
                                <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/instagram_img.svg') }}" class="socail_icon"> </a> </span>
                                <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/whatsapp.svg') }}" class="socail_icon"> </a> </span>
                                <span class="post_share_item"> <a href="#" class="text-decoration-none"> <img src="{{ asset('images/youtube.svg') }}" class="socail_icon"> </a> </span>
                            </div>
                        </div>
                        <div class="mt-auto">
                            <p class="banner_header">subscribe with us</p>
                            <form method="post" class="banner_form">
                                <div class="d-flex justify-content-center">
                                    <input type="email" class="banner_box" placeholder="Valid Email Address">
                                    <button type="submit" class="banner_btn"> <i class="fa fa-arrow-right"></i> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="aside_post_header_con border-bottom">
                            <p class="aside_post_header">Trending Post</p>
                        </div>
                        @forelse($favs as $fav)
                            <div class="d-flex mb-3">
                                <img src="{{ asset('storage/'.$fav->image) }}" class="post_img_sm mr-3">
                                <div class="col px-0">
                                    <a href="{{ route('read', ['id'=>$fav->id]) }}" class="text-decoration-none"> <p class="post_text">{{$fav->title }}</p> </a>
                                    <div class="d-flex justify-content-between">
                                        <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                        <a href="{{ route('posts.subcategory', ['id'=>$fav->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$fav->subcategory->name}} {{$fav->category->name}}</span></a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p class="post_text text-center mb-3">No Record To Show</p>
                        @endforelse
                    </div>
                </div>
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="{{ asset('images/advert.gif') }}" class="advert1">
                        </div>
                    </div>
                    <div class="col pr-0 pl-0 pl-md-3 pl-lg-0">
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">song of the week</p>
                            </div>
                            <a href="reading.blade.php" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">10</span> <span class="video_icon"> <i class="fa fa-music"></i> </span>
                                    <img src="{{ asset('images/music_img1.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="#" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-play"></i> video</span></a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">artiste of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">2.9k</span>
                                    <img src="{{ asset('images/artist_img.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">blog queen of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <img src="{{ asset('images/artist_img.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="{{ asset('images/advert.gif') }}" class="advert1">
                        </div>
                    </div>
                </div>
            </div>
            <!--second content container ENDS-->
        </div>
    </div>
    <!--body container ENDS-->
@endsection

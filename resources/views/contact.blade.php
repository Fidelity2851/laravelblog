@extends('layouts/layout')

@section('title')
    Contact
@endsection

@section('content')
    <!--body container-->
    <div class="body_con p-0">

        <!--contact container-->
        <div class="contact_con">
            <div class="container contact px-md-0">
                <div class=" message_con px-0 mb-5">
                    <div class="message_form_con">
                        <p class="message_header">send us a message</p>
                        <?php
                            if(isset($good)) {
                                ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                    <strong>
                                        <?php
                                        if (isset($good)) {
                                            echo $good;
                                        }
                                        ?>
                                    </strong>
                                </div>
                                <?php
                            }
                        ?>
                        <?php
                            if(isset($bad)) {
                            ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <strong>
                                    <?php
                                    if (isset($bad)) {
                                        echo $bad;
                                    }
                                    ?>
                                </strong>
                            </div>
                            <?php
                        }
                        ?>
                        <form action="" enctype="multipart/form-data" class="message_form" method="post">
                            <div class="d-md-flex justify-content-between">
                                <div class="col mr-md-5 mb-3 px-0">
                                    <label class="contact_label">name:</label>
                                    <input type="text" name="name" class="contact_box" required>
                                </div>
                                <div class="col mb-3 px-0">
                                    <label class="contact_label">email:</label>
                                    <input type="email" name="email" class="contact_box" required>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label class="contact_label">message:</label>
                                <textarea class="message_box" name="complain" required></textarea>
                            </div>
                            <div class="message_btn_con">
                                <button type="reset" class="reset_btn mr-4">reset</button>
                                <button type="submit" name="submit_btn" class="submit_btn3">submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="map_con mb-5 px-0">
                    <iframe class="map" id="gmap_canvas" src="https://maps.google.com/maps?q=ngwo%20asaa&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
            </div>
        </div>
        <!--contact container ENDS-->
    </div>
    <!--body container ENDS-->
@endsection

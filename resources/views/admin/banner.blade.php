<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Banner
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">banner</p>
            <div class="flex-self-center">
                <p class="dis_bind_act">Banner</p>
            </div>
        </div>


        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif

            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Create Banner</a></li>
                <li><a href="#_target_2" class="tab_link">Manger Banner</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="{{ route('banner.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Banner Title</label>
                                    <input type="text" value="{{old('title')}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                    @error('title')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Banner image</label>
                                    <input type="file" value="{{old('image')}}" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" required>
                                    @error('image')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{old('description')}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="_target_2">
                    <livewire:admin.banner />
                </div>
            </div>
        </div>
    </div>
@endsection


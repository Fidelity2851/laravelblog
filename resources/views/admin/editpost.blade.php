<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Post - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit post</p>
            <div class="d-flex flex-self-center">
                <a href="{{ route('post.list') }}" class="no-decor mr-3"> <p class="dis_bind_act">post</p> </a>
                <p class="dis_bind_act">Editpost</p>
            </div>
        </div>


        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif

            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Post</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="{{ route('post.update', ['post' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Post Title</label>
                                        <input type="text" value="{{$query->title}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                        @error('title')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Slug/URl (Optional)</label>
                                        <input type="text" value="{{$query->slug}}" name="slug" class="post_box @error('slug') is-invalid @enderror" data-role="input">
                                        @error('slug')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-justify-between">
                                    <div class="col-3 ">
                                        <label class="post_label">Post image</label>
                                        <input type="file" value="{{$query->image}}" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" >
                                        @error('image')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                        @if($query->image)
                                            <div class="p-3">
                                                <img class="post_img" src="{{asset('storage/'.$query->image)}}">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-3 ">
                                        <label class="post_label">Musics & Videos</label>
                                        <input type="file" value="{{old('file')}}" name="file" class="post_box @error('file') is-invalid @enderror" data-role="file">
                                        @error('file')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                        @if($query->file)
                                            <div class="pt-3">
                                                <audio src="{{ asset('storage/'.$query->file) }}" controls></audio>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-3 ">
                                        <label class="post_label">Category</label>
                                        <select id="category" name="category" class="post_sel @error('category') is-invalid @enderror" data-role="select" required>
                                            <option value="{{$query->category->id}}" selected>{{$query->category->name}}</option>
                                            @foreach($query1 as $query1)
                                                <option value="{{$query1->id}}">{{$query1->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('category')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col-3 ">
                                        <label class="post_label">Sub-category</label>
                                        <select id="subcategory" name="subcategory" class="post_sel sub_cate @error('subcategory') is-invalid @enderror" required>
                                            <option value="{{$query->subcategory->id}}" selected>{{$query->subcategory->name}}</option>
                                           {{--@foreach($query2 as $query2)
                                                <option value="{{$query2->id}}">{{$query2->name}}</option>
                                           @endforeach--}}
                                        </select>
                                        <span class="feedback">Select a Category first</span>
                                        @error('subcategory')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-justify-between">
                                    <div class="col-6 ">
                                        <label class="post_label">Summary</label>
                                        <textarea name="summary" class="post_area @error('summary') is-invalid @enderror" data-role="textarea" required>{{$query->summary}}</textarea>
                                        @error('summary')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Tags</label>
                                        <input type="text" value="{{$tag}}, " name="tags" class="post_box @error('tags') is-invalid @enderror" data-role="taginput" required>
                                        @error('tags')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Publision Date</label>
                                        <input type="text" name="pub_date" class="post_box @error('pub_date') is-invalid @enderror" data-role="calendarpicker" data-dialog-mode="true" value="{{$query->pub_at}}">
                                        @error('pub_date')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea id="editor" name="description" class="post_area @error('description') is-invalid @enderror" required>{{$query->description}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

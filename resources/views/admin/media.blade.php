<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Media
@endsection

<!--display-->
@section('content')
        <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">media</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">Media</p>
                </div>
            </div>


            <div class="post_con p-4">
                @if(session()->has('msg'))
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                @endif

                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Media</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Media</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                            <div class="col-5 p-0">
                                <form class="post_form" action="{{ route('media.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col ">
                                        <label class="post_label">Media Title</label>
                                        <input type="text" value="{{old('title')}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                        @error('title')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Cover Image</label>
                                        <input type="file" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" required>
                                        @error('image')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Description</label>
                                        <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{old('description')}}</textarea>
                                        @error('description')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                        <button type="submit" class="post_btn2">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-5 p-0">
                                <form class="post_form" action="{{ route('gallery.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="col ">
                                        <label class="post_label">Media Title</label>
                                        <select name="media_title" class="post_sel @error('media_title') is-invalid @enderror" data-role="select" required>
                                            <option disabled selected>Select a Media Title</option>
                                            @foreach($medias as $data)
                                            <option value="{{$data->id}}">{{$data->title}}</option>
                                            @endforeach
                                        </select>
                                        @error('media_title')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Photos (Mulit select)</label>
                                        <input type="file" name="photos" class="post_box @error('photos') is-invalid @enderror" data-role="file" multiple required>
                                        @error('photos')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                        <button type="submit" class="post_btn2">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="_target_2">
                        <form method="post" action="{{ route('media.deletemany') }}">
                            @csrf
                            <table class="table table-border row-border compact striped"
                                   data-check="false"
                                   data-show-search="false"
                                   data-show-rows-steps="false"
                                   data-horizontal-scroll="false"
                                   data-show-pagination="false"
                            >
                                <thead>
                                <tr>
                                    <th class="t_head" > <input type="checkbox" name="check_all" data-role="checkbox"> </th>
                                    <th class="t_head" data-sortable="true">ID</th>
                                    <th class="t_head" data-sortable="true">Title</th>
                                    <th class="t_head" data-sortable="true">Cover Image</th>
                                    <th class="t_head" data-sortable="true">Description</th>
                                    <th class="t_head" data-sortable="true">Created date</th>
                                    <th class="t_head">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($medias as $media)
                                    <tr>
                                        <td class="t_data"><input value="{{$media->id}}" name="checked[]" type="checkbox" data-role="checkbox"></td>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$media->title}}</td>
                                        <td><img class="post_img" src="{{asset('storage/'.$media->image)}}"></td>
                                        <td>{{$media->description}}</td>
                                        <td>{{$media->created_at}}</td>
                                        <td>
                                            <a href="{{ route('media.fetch', ['media' => $media->id]) }}" title="Edit" class="mif-open-book t_icon"></a>
                                            <a href="{{ route('media.delete', ['media' => $media->id]) }} " title="Delete" class="mif-bin t_icon con_del"></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="">
                                <button type="submit" class="del_all_btn mb-3">Delete</button>
                                {{ $medias->links() }}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endsection

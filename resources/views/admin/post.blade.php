<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Post
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
            <div class="col dis_head d-flex flex-justify-between px-4">
                <p class="dis_header flex-self-center">post</p>
                <div class="flex-self-center">
                    <p class="dis_bind_act">post</p>
                </div>
            </div>


            <div class="post_con p-4">

                @if(session()->has('msg'))
                    <div class="msg_con">
                        <div class="msg d-flex flex-justify-between flex-self-start">
                            <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                            <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session('msg') }}</p>
                            <button type="button" class="msg_btn flex-self-center">&times;</button>
                        </div>
                    </div>
                @endif

                <ul data-role="tabs" data-expand="true">
                    <li><a href="#_target_1" class="tab_link">Create Post</a></li>
                    <li><a href="#_target_2" class="tab_link">Manger Post</a></li>
                </ul>
                <div class="border bd-default no-border-top p-2">
                    <div id="_target_1">
                        <div class="d-flex flex-justify-between">
                                <div class="col p-0">
                                    <form class="post_form" action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="d-flex flex-justify-between">
                                            <div class="col ">
                                                <label class="post_label">Post Title</label>
                                                <input type="text" value="{{old('title')}}" name="title" class="post_box @error('title') is-invalid @enderror" data-role="input" required>
                                                @error('title')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Slug/URl (Optional)</label>
                                                <input type="text" value="{{old('slug')}}" name="slug" class="post_box @error('slug') is-invalid @enderror" data-role="input" >
                                                @error('slug')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col-3 ">
                                                <label class="post_label">Post image</label>
                                                <input type="file" value="{{old('image')}}" name="image" class="post_box @error('image') is-invalid @enderror" data-role="file" required>
                                                @error('image')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Musics & Videos</label>
                                                <input type="file" value="{{old('file')}}" name="file" class="post_box @error('file') is-invalid @enderror" data-role="file">
                                                @error('file')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Category</label>
                                                <select id="category" name="category" class="post_sel cate @error('category') is-invalid @enderror" data-role="select" required>
                                                    <option disabled selected>select a category</option>
                                                    @foreach($query1 as $data1)
                                                        <option value="{{$data1->id}}">{{$data1->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('category')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col-3 ">
                                                <label class="post_label">Sub-category</label>
                                                <select name="subcategory" class="post_sel sub_cate @error('subcategory') is-invalid @enderror" required>
                                                    {{--@foreach($query2 as $data2)
                                                        <option value="{{$data2->id}}">{{$data2->name}}</option>
                                                    @endforeach--}}
                                                </select>
                                                <span class="feedback">Select a Category first</span>
                                                @error('subcategory')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="d-flex flex-justify-between">
                                            <div class="col-6 ">
                                                <label class="post_label">Summary</label>
                                                <textarea name="summary" class="post_area @error('summary') is-invalid @enderror" data-role="textarea">{{old('summary')}}</textarea>
                                                @error('summary')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Tags</label>
                                                <input type="text" value="{{old('tags')}}" name="tags" class="post_box @error('tags') is-invalid @enderror" data-role="taginput" required>
                                                @error('tags')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                            <div class="col ">
                                                <label class="post_label">Publision Date</label>
                                                <input type="text" name="pub_date" class="post_box @error('pub_date') is-invalid @enderror" data-role="calendarpicker" data-dialog-mode="true" value="<?php echo now(); ?>">
                                                @error('pub_date')
                                                <p class="error_msg">{{ $message }}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col ">
                                            <label class="post_label">Description</label>
                                            <textarea id="editor" name="description" class="post_area @error('description') is-invalid @enderror">{{old('description')}}</textarea>
                                            @error('description')
                                            <p class="error_msg">{{ $message }}</p>
                                            @enderror
                                        </div>
                                        <div class="col ">
                                            <button type="reset" class="post_btn1 mr-4" data-role="reset">Reset</button>
                                            <button type="submit" class="post_btn2">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                    <div id="_target_2">
                        <livewire:admin.post />
                    </div>
                </div>
            </div>
        </div>
@endsection

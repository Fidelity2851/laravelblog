<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Users - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit users</p>
            <div class="flex-self-center d-flex">
                <a href="{{ route('users.list') }}" class="no-decor mr-3"> <p class="dis_bind_act">users</p> </a>
                <p class="dis_bind_act">Editusers</p>
            </div>
        </div>

        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif
            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Users</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col p-0">
                            <form class="post_form" action="{{ route('users.update', ['user' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Username</label>
                                        <input type="text" value="{{$query->username}}" name="username" class="post_box  @error('username') is-invalid @enderror" data-role="input" required>
                                        @error('username')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Email Address</label>
                                        <input type="Email" value="{{$query->email}}" name="email" class="post_box  @error('email') is-invalid @enderror" data-role="input" required>
                                        @error('email')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">User Role</label>
                                        <select name="role" class="post_sel  @error('role') is-invalid @enderror" data-role="select" required>
                                            <option value="{{$query->role}}" selected>{{$query->role}}</option>
                                            @can('create', App\User::class)
                                                <option value="user">User</option>
                                            @endcan
                                            @can('viewAny', App\User::class)
                                                <option value="writter">Writter</option>
                                            @endcan
                                            @can('create', App\User::class)
                                                <option value="admin">Admin</option>
                                            @endcan
                                        </select>
                                        @error('role')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="d-flex flex-justify-between">
                                    <div class="col ">
                                        <label class="post_label">Profile photo</label>
                                        <input type="file" name="image" class="post_box  @error('image') is-invalid @enderror" data-role="file">
                                        @error('image')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                        <div class="p-3">
                                            <img class="post_img" src="{{asset('storage/'.$query->image)}}">
                                        </div>
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Password</label>
                                        <input type="password" name="password" class="post_box  @error('password') is-invalid @enderror" data-role="input">
                                        @error('password')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                    <div class="col ">
                                        <label class="post_label">Comfirm Password</label>
                                        <input type="password" name="comfirm_password" class="post_box  @error('comfirm_password') is-invalid @enderror" data-role="input">
                                        @error('comfirm_password')
                                        <p class="error_msg">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col ">
                                    <label class="post_label">Short Biograph</label>
                                    <textarea name="biograph" class="post_area @error('biograph') is-invalid @enderror" data-role="textarea" required>{{$query->biograph}}</textarea>
                                    @error('biograph')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

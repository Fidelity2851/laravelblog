<!--dashboard bar-->
@extends('layouts.header')

<!--title-->
@section('title')
    Category - Edit
@endsection

<!--display-->
@section('content')
    <div class="col-10 dis_con pos-absolute p-0">
        <div class="col dis_head d-flex flex-justify-between px-4">
            <p class="dis_header flex-self-center">edit Sub category</p>
            <div class="d-flex flex-self-center">
                <a href="{{ route('category.list') }}" class="no-decor mr-3"> <p class="dis_bind_act">category</p> </a>
                <p class="dis_bind_act">Edit sub-category</p>
            </div>
        </div>


        <div class="post_con p-4">
            @if(session()->has('msg'))
                <div class="msg_con">
                    <div class="msg d-flex flex-justify-between flex-self-start">
                        <span class="msg_icon default-icon-check flex-self-center mr-5"></span>
                        <p class="msg_text flex-self-center mr-10"><strong>Success!</strong> {{ session()->get('msg') }}</p>
                        <button type="button" class="msg_btn flex-self-center">&times;</button>
                    </div>
                </div>
            @endif


            <ul data-role="tabs" data-expand="true">
                <li><a href="#_target_1" class="tab_link">Edit Sub-Category</a></li>
            </ul>
            <div class="border bd-default no-border-top p-2">
                <div id="_target_1">
                    <div class="d-flex flex-justify-between">
                        <div class="col-5 ">
                            @foreach($query as $query)
                            <form class="post_form" action="{{ route('subcategory.update', ['subcategory' => $query->id]) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="col ">
                                    <label class="post_label">Sub Category Name</label>
                                    <input type="text" value="{{$query->name}}" name="subcategory" class="post_box @error('subcategory') is-invalid @enderror" data-role="input" required>
                                    @error('subcategory')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <label class="post_label">Description</label>
                                    <textarea name="description" class="post_area @error('description') is-invalid @enderror" data-role="textarea" required>{{$query->description}}</textarea>
                                    @error('description')
                                    <p class="error_msg">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div class="col ">
                                    <button type="submit" class="post_btn2">Update</button>
                                </div>
                            </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

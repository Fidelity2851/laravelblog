@extends('layouts/layout')

@section('title')
    About
@endsection

@section('content')
    <!--body container-->
    <div class="row body_con mt-3 mx-0">
        <div class="container body_content_con d-lg-flex justify-content-between px-0">
            <!--first content container-->
            <div class="col-12 col-lg-8 body1 border-right-0 pl-0 pr-0 pr-lg-3">
                <!--latest post container-->
                <div class="mb-5">
                    <div class="latest_header_con d-flex justify-content-center">
                        <p class="latest_header align-self-center">abouts us</p>
                    </div>
                    <div class="">

                    </div>
                </div>
                <!--latest post container ENDS-->
            </div>
            <!--first content container ENDS-->

            <!--second content container -->
            <div class="col-12 col-lg d-md-flex d-lg-block body2 pr-md-0 pl-md-0 pl-lg-3">
                <div class="col px-0">
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="mb-3">
                            <p class="banner_header">connect with us</p>
                            <div class="d-flex ">
                                <span class=""> <a href="#" class="text-decoration-none"> <img src="images/facebook.svg" class="socail_icon"> </a> </span>
                                <span class=""> <a href="#" class="text-decoration-none"> <img src="images/twitter.svg" class="socail_icon"> </a> </span>
                                <span class=""> <a href="#" class="text-decoration-none"> <img src="images/instagram_img.svg" class="socail_icon"> </a> </span>
                                <span class=""> <a href="#" class="text-decoration-none"> <img src="images/whatsapp.svg" class="socail_icon"> </a> </span>
                                <span class=""> <a href="#" class="text-decoration-none"> <img src="images/youtube.svg" class="socail_icon"> </a> </span>
                            </div>
                        </div>
                        <div class="mt-auto">
                            <p class="banner_header">subscribe with us</p>
                            <form method="post" class="banner_form">
                                <div class="d-flex justify-content-center">
                                    <input type="email" class="banner_box" placeholder="Valid Email Address">
                                    <button type="submit" class="banner_btn"> <i class="fa fa-arrow-right"></i> </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="aside_post_header_con border-bottom">
                            <p class="aside_post_header">Trending Post</p>
                        </div>
                        @forelse($favs as $fav)
                            <div class="d-flex mb-3">
                                <img src="{{ asset('storage/'.$fav->image) }}" class="post_img_sm mr-3">
                                <div class="col px-0">
                                    <a href="{{ route('read', ['id'=>$fav->id]) }}" class="text-decoration-none"> <p class="post_text">{{$fav->title }}</p> </a>
                                    <div class="d-flex justify-content-between">
                                        <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                        <a href="{{ route('posts.subcategory', ['id'=>$fav->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$fav->subcategory->name}} {{$fav->category->name}}</span></a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p class="post_text text-center mb-3">No Record To Show</p>
                        @endforelse
                    </div>
                </div>
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                    <div class="col pr-0 pl-0 pl-md-3 pl-lg-0">
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">song of the week</p>
                            </div>
                            <a href="reading.blade.php" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">10</span> <span class="video_icon"> <i class="fa fa-music"></i> </span>
                                    <img src="images/music_img1.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="#" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-play"></i> video</span></a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">artiste of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">2.9k</span>
                                    <img src="images/artist_img.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">blog queen of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <img src="images/artist_img.jpg" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="images/advert.gif" class="advert1">
                        </div>
                    </div>
                </div>

            </div>
            <!--second content container ENDS-->
        </div>
    </div>
    <!--body container ENDS-->
@endsection

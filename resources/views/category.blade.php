@extends('layouts/layout')

@section('title')
    Category
@endsection

@section('content')
    <!--banner-->
    @include('banner')

    <!--body container-->
    <div class="row body_con mt-3 mx-0">
        <div class="container body_content_con d-lg-flex justify-content-between px-0">
            <!--first content container-->
            <div class="col-12 col-lg-8 body1 border-right-0 pl-0 pr-0 pr-lg-3">
                <!--latest post container-->
                <div class="mb-5">
                    <div class="latest_header_con d-flex justify-content-center">
                        @foreach($posts as $post)
                            @if($loop->first)
                                <p class="latest_header align-self-center">Latest {{$post->category->name}}</p>
                            @endif
                        @endforeach
                    </div>
                    <div class="">
                        <div class="col post_row px-md-0">
                            @foreach($posts as $post)
                                <div class="col latest_post d-sm-flex justify-content-between px-0">
                                    <div class="col post_img_con px-0">
                                        <span class="view" title="Views">{{ $post->views }}</span> <span class="video_icon"> <i class="fa fa-music"></i> </span>
                                        <img src="{{ asset('../storage/'.$post->image) }}" class="post_img">
                                    </div>
                                    <div class="col-12 col-sm-7 post_detail px-0 px-sm-4">
                                        <a href="{{ route('read', ['id'=>$post->id]) }}" class="text-decoration-none"> <p class="post_text_lg">{{ $post->title }}</p> </a>
                                        <p class="post_desc">{{ strip_tags($post->summary) }}</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> {{ date('d/M/Y') }} </span>
                                            <a href="{{ route('posts.subcategory', ['id'=>$post->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{ $post->subcategory->name }} {{ $post->category->name }}</span></a>
                                        </div>
                                        <button type="button" class="play">play now</button>
                                    </div>
                                </div>
                            @endforeach

                            <div class="d-none d-md-block advert_con mb-5">
                                <div class="advert2">
                                    <img src="{{ asset('images/advert.gif') }}" class="advert2">
                                </div>
                            </div>
                            <div class="d-md-none advert_con mb-5">
                                <div class="advert3">
                                    <img src="{{ asset('images/advert.gif') }}" class="advert3">
                                </div>
                            </div>

                        </div>
                        <div class="pagi_con d-flex justify-content-center">
                            {{ $posts->links() }}
                            {{--<div class="col-12 col-sm-8 col-md-6 pagi d-flex justify-content-between">
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> &#10094;</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item_active"> 1</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> 2</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> 3</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> 4</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> ...</p> </a>
                                <a href="#" class="text-decoration-none"> <p class="pagi_item"> &#10095;</p> </a>
                            </div>--}}
                        </div>
                    </div>
                </div>
                <!--latest post container ENDS-->
            </div>
            <!--first content container ENDS-->

            <!--second content container -->
            <div class="col-12 col-lg d-md-flex d-lg-block body2 pr-md-0 pl-md-0 pl-lg-3">
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="{{ asset('images/advert.gif') }}" class="advert1">
                        </div>
                    </div>
                    <div class="col mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="aside_post_header_con border-bottom">
                            <p class="aside_post_header">Trending Post</p>
                        </div>
                        @forelse($favs as $fav)
                            <div class="d-flex mb-3">
                                <img src="{{ asset('storage/'.$fav->image) }}" class="post_img_sm mr-3">
                                <div class="col px-0">
                                    <a href="{{ route('read', ['id'=>$fav->id]) }}" class="text-decoration-none"> <p class="post_text">{{$fav->title }}</p> </a>
                                    <div class="d-flex justify-content-between">
                                        <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                        <a href="{{ route('posts.subcategory', ['id'=>$fav->subcategory->id]) }}" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-music"></i> {{$fav->subcategory->name}} {{$fav->category->name}}</span></a>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <p class="post_text text-center mb-3">No Record To Show</p>
                        @endforelse
                    </div>
                </div>
                <div class="col px-0">
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="{{ asset('images/advert.gif') }}" class="advert1">
                        </div>
                    </div>
                    <div class="col pr-0 pl-0 pl-md-3 pl-lg-0">
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">song of the week</p>
                            </div>
                            <a href="reading.blade.php" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">10</span> <span class="video_icon"> <i class="fa fa-music"></i> </span>
                                    <img src="{{ asset('images/music_img1.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                        <div class="d-flex justify-content-between">
                                            <span class="time align-self-center"> <i class="fa fa-clock"></i> 2 mins ago </span>
                                            <a href="#" class="text-decoration-none align-self-center"> <span class="type"> <i class="fa fa-play"></i> video</span></a>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">artiste of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <span class="view">2.9k</span>
                                    <img src="{{ asset('images/artist_img.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col mb-5 px-0">
                            <div class="aside_post_header_con border-bottom">
                                <p class="aside_post_header">blog queen of the week</p>
                            </div>
                            <a href="#" class="text-decoration-none">
                                <div class="video_post_con">
                                    <img src="{{ asset('images/artist_img.jpg') }}" class="post_img_md">
                                    <div class="video_text_con">
                                        <p class="post_text_md">Dua Lipa Has A Treat To Rainy Festival With Her Fashionable Outfit</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col advert_con mb-5 pl-0 pr-0 pr-md-3 pr-lg-0">
                        <div class="advert1">
                            <img src="{{ asset('images/advert.gif') }}" class="advert1">
                        </div>
                    </div>
                </div>
            </div>
            <!--second content container ENDS-->
        </div>
    </div>
    <!--body container ENDS-->

@endsection
